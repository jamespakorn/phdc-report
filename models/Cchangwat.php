<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "cchangwat".
 *
 * @property string $changwatcode
 * @property string $changwatname
 * @property string $zonecode
 */
class Cchangwat extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cchangwat';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['changwatcode'], 'required'],
            [['changwatcode', 'zonecode'], 'string', 'max' => 2],
            [['changwatname'], 'string', 'max' => 255],
            [['changwatcode'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'changwatcode' => 'Changwatcode',
            'changwatname' => 'Changwatname',
            'zonecode' => 'Zonecode',
        ];
    }

    public static function GetList()
    {
        return ArrayHelper::map(self::find()->all(), 'changwatcode', 'changwatname');
    }

    public static function GetListCode($code)
    {
        return ArrayHelper::map(self::find()
                ->where('changwatcode=:code', [':code' => $code])
                ->all(), 'changwatcode', 'changwatname');
    }

    public static function GetChangwatName($id)
    {
        if (($model = self::findOne($id)) !== null) {
            return $model->changwatname;
        } else {
            return 'ไม่มีข้อมูลจังหวัด';
        }
       
    }
}
