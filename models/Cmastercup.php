<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "cmastercup".
 *
 * @property int $oid
 * @property string $province_id จังหวัด
 * @property string $hospcode5
 * @property string $hmain แม่ข่าย
 * @property string $hsub ลูกข่าย
 * @property string $hmainname ชื่อแม่ข่าย
 * @property string $hmaintype ประเภทโรงพยาบาลแม่ข่าย
 * @property string $d_update
 * @property string $changwatcode
 * @property string $changwatname
 * @property string $ampurcode
 * @property string $ampurname
 * @property string $tamboncode
 * @property string $tambonname
 * @property string $villagecode
 * @property string $village
 * @property string $address
 * @property string $postcode
 * @property string $catma
 * @property string $catm
 * @property string $datecancelcode
 * @property string $edit_time
 * @property string $hospcode9
 * @property string $is_pcu
 * @property string $area_code
 * @property string $hsubname
 */
class Cmastercup extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cmastercup';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['oid', 'hsub', 'hsubname'], 'required'],
            [['oid'], 'integer'],
            [['edit_time'], 'safe'],
            [['province_id'], 'string', 'max' => 9],
            [['hospcode5', 'hmain', 'hsub', 'hmaintype', 'changwatcode', 'ampurcode', 'tamboncode', 'villagecode', 'address', 'postcode', 'hospcode9'], 'string', 'max' => 13],
            [['hmainname', 'changwatname', 'ampurname', 'tambonname', 'village', 'catma', 'hsubname'], 'string', 'max' => 255],
            [['d_update', 'catm', 'datecancelcode'], 'string', 'max' => 18],
            [['is_pcu'], 'string', 'max' => 1],
            [['area_code'], 'string', 'max' => 12],
            [['hsub'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'oid' => 'Oid',
            'province_id' => 'Province ID',
            'hospcode5' => 'Hospcode5',
            'hmain' => 'Hmain',
            'hsub' => 'Hsub',
            'hmainname' => 'Hmainname',
            'hmaintype' => 'Hmaintype',
            'd_update' => 'D Update',
            'changwatcode' => 'Changwatcode',
            'changwatname' => 'Changwatname',
            'ampurcode' => 'Ampurcode',
            'ampurname' => 'Ampurname',
            'tamboncode' => 'Tamboncode',
            'tambonname' => 'Tambonname',
            'villagecode' => 'Villagecode',
            'village' => 'Village',
            'address' => 'Address',
            'postcode' => 'Postcode',
            'catma' => 'Catma',
            'catm' => 'Catm',
            'datecancelcode' => 'Datecancelcode',
            'edit_time' => 'Edit Time',
            'hospcode9' => 'Hospcode9',
            'is_pcu' => 'Is Pcu',
            'area_code' => 'Area Code',
            'hsubname' => 'Hsubname',
        ];
    }

    public static function GetMastercup($changwatcode)
    {
        return ArrayHelper::map(self::find()
            // ->select(['hmain', 'hmainname'])
            // ->distinct()
                ->where('left(province_id,2)=:changwatcode', ['changwatcode' => $changwatcode])
                ->all(), 'hmain', 'hmainname');
    }


    public static function GetMastercupname($hmain)
    {
        if (($model = self::find()->where(['hmain' => $hmain])->one()) !== null) {
            return ' CUP ' . $model->hmainname;
        } else {
            return null;
        }
    }
}
