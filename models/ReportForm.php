<?php

namespace app\models;

use yii\base\Model;

class ReportForm extends Model
{
    public $byear;
    public $eyear;
    public $tyear;
    public $start_d;
    public $end_d;
    public $changwatcode;
    public $ampurcode;
    public $ampurstyle;
    public $tamboncode;
    public $mastercup;
    public $ageMin;
    public $ageMax;
    public $class;
    public $pteeth1;
    public $pteeth2;
    public $ppp;
    public $school;
    public $cupstyle;

    public function rules()
    {
        return [
            [['byear','changwatcode'], 'required'],
            [['ageMin','ageMax'], 'integer','min'=>0],
            [['ageMin','ageMax'], 'integer','max'=>200],
            // [['ampurcode','ampurstyle'], 'isAmpurstyle'],

        ];
    }

    public function attributeLabels()
    {
        return [
            'byear' => 'ปีงบประมาณ',
            'eyear' => 'ปีการศึกษา',
            'changwatcode' => 'จังหวัด',
            'ampurcode' => 'อำเภอ/เขต',
            'ampurstyle' => 'ตำบล/แขวง/หน่วยบริการ/โรงเรียน',
            'tamboncode'=>'ตำบล/แขวง',
            'mastercup'=>'เครือข่ายบริการ',
            'ageMin'=>'ช่วงอายุ(ปี)',
            'ageMax'=>'ถึง(ปี)',
            'class'=>'ชั้น',
            'school'=>'โรงเรียน',
            'cupstyle'=>'หน่วยบริการ/โรงเรียน'
        ];
    }


    // public function isAmpurstyle($attribute, $params) {
    //     if ($this->ampurcode !== '' && $this->ampurstyle == null) {
    //         $this->addError($attribute, 'กรุณาเลือกตำบล/แขวง/หน่วยบริการ');
    //     }
    // }

}
