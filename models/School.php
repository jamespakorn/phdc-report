<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "school".
 *
 * @property string $hospcode
 * @property string $vid
 * @property string $schoolcode
 * @property string $schoolname
 * @property string $schoolid
 * @property string $schoolowner
 * @property string $schooltype
 * @property string $closedate
 * @property string $d_update
 * @property string $depend
 * @property string $maxclass
 */
class School extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'school_r11';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['hospcode', 'vid', 'schoolcode'], 'required'],
            [['closedate', 'd_update'], 'safe'],
            [['hospcode', 'schoolcode', 'schoolname', 'depend', 'maxclass'], 'string', 'max' => 255],
            [['vid'], 'string', 'max' => 8],
            [['schoolid'], 'string', 'max' => 15],
            [['schoolowner'], 'string', 'max' => 2],
            [['schooltype'], 'string', 'max' => 1],
            [['hospcode', 'schoolcode'], 'unique', 'targetAttribute' => ['hospcode', 'schoolcode']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'hospcode' => 'Hospcode',
            'vid' => 'Vid',
            'schoolcode' => 'Schoolcode',
            'schoolname' => 'Schoolname',
            'schoolid' => 'Schoolid',
            'schoolowner' => 'Schoolowner',
            'schooltype' => 'Schooltype',
            'closedate' => 'Closedate',
            'd_update' => 'D Update',
            'depend' => 'Depend',
            'maxclass' => 'Maxclass',
        ];
    }

    public function getChospital()
    {
        return $this->hasOne(Chospital::className(), ['hoscode' => 'hospcode']);
    }


    public static function GetList()
    {
        return \yii\helpers\ArrayHelper::map(self::find()
        ->select(['concat(hospcode,"-",schoolcode) as schoolcode','concat(schoolname," | ",h.hoscode," | ",h.hosname) as schoolname'])
        ->leftJoin('chospital h', 'h.hoscode = hospcode')
        ->orderBy('h.hoscode')
        ->all(), 'schoolcode', 'schoolname');
    }

}
