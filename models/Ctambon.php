<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "ctambon".
 *
 * @property string $tamboncode
 * @property string $tambonname
 * @property string $tamboncodefull
 * @property string $ampurcode
 * @property string $changwatcode
 * @property string $flag_status สถานนะของพื้นที่ 0=ปกติ 1=เลิกใช้(แยก/ย้ายไปพื้นที่อื่น)
 */
class Ctambon extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ctambon';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tamboncode', 'tamboncodefull', 'ampurcode', 'changwatcode'], 'required'],
            [['tamboncode', 'changwatcode'], 'string', 'max' => 2],
            [['tambonname'], 'string', 'max' => 255],
            [['tamboncodefull'], 'string', 'max' => 6],
            [['ampurcode'], 'string', 'max' => 4],
            [['flag_status'], 'string', 'max' => 1],
            [['tamboncode', 'tamboncodefull', 'ampurcode', 'changwatcode'], 'unique', 'targetAttribute' => ['tamboncode', 'tamboncodefull', 'ampurcode', 'changwatcode']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'tamboncode' => 'Tamboncode',
            'tambonname' => 'Tambonname',
            'tamboncodefull' => 'Tamboncodefull',
            'ampurcode' => 'Ampurcode',
            'changwatcode' => 'Changwatcode',
            'flag_status' => 'Flag Status',
        ];
    }

    public static function GetList($ampurcode)
    {
        return ArrayHelper::map(self::find()
                ->where(['ampurcode' => $ampurcode])
                ->all(), 'tamboncodefull', 'tambonname');
    }

    public static function GetTambon($tamboncodefull)
    {
        return ArrayHelper::map(self::find()
                ->where(['tamboncodefull' => $tamboncodefull])->all(), 'tamboncodefull', 'tambonname');
    }

    public static function GetTambonname($tamboncodefull)
    {
        if (($model = self::find()->where(['tamboncodefull' => $tamboncodefull])->one()) !== null) {
            return ' ต.' . $model->tambonname;
        } else {
            return null;
        }
    }

}
