<?php

namespace app\components;

/**
 * Description of Thaidate
 *
 * @author jamespakorn
 */
class Thaidate extends \yii\base\Component {

    public $monthFull = [
        "01" => "มกราคม",
        "02" => "กุมภาพันธ์",
        "03" => "มีนาคม",
        "04" => "เมษายน",
        "05" => "พฤษภาคม",
        "06" => "มิถุนายน",
        "07" => "กรกฎาคม",
        "08" => "สิงหาคม",
        "09" => "กันยายน",
        "10" => "ตุลาคม",
        "11" => "พฤศจิกายน",
        "12" => "ธันวาคม"
    ];
    public $monthShort = [
        "01" => "ม.ค.",
        "02" => "ก.พ.",
        "03" => "มี.ค.",
        "04" => "เม.ย.",
        "05" => "พ.ค.",
        "06" => "มิ.ย.",
        "07" => "ก.ค.",
        "08" => "ส.ค.",
        "09" => "ก.ย.",
        "10" => "ต.ค.",
        "11" => "พ.ย.",
        "12" => "ธ.ค."
    ];
    public $monthNumber = [
        "01" => "1",
        "02" => "2",
        "03" => "3",
        "04" => "4",
        "05" => "5",
        "06" => "6",
        "07" => "7",
        "08" => "8",
        "09" => "9",
        "10" => "10",
        "11" => "11",
        "12" => "12"
    ];

    public function Date($date, $type) {

        $ex = explode("-", $date);

        if (count($ex) == 3) {

            $cdate = checkdate($ex[1], $ex[2], $ex[0]);
            if ($cdate) {

                switch ($type) {
                    case 'f':
                        return intval($ex[2]) . ' ' .
                                $this->monthFull[$ex[1]] . ' พ.ศ. ' . (intval($ex[0]) + 543);
                        break;
                    case 's':
                        return intval($ex[2]) . ' ' .
                                $this->monthShort[$ex[1]] . ' ' . (intval($ex[0]) + 543);
                        break;
                    case 'n':
                        return intval($ex[2]) . '/' .
                                $this->monthNumber[$ex[1]] . '/' . (intval($ex[0]) + 543);
                        break;
                    default:
                        echo 'รูปแบบไม่ตรงที่กำหนด';
                        break;
                }
            } else {
                return 'วันที่ไม่ถูกต้อง';
            }
        } else {
            return 'ข้อมูลไม่ถูกต้อง';
        }
    }

    # Set time format 

    public function Time($time, $type) {

        switch ($type) {
            case 'f':
                echo substr($time, 0, -6) . ':' .
                substr($time, 3, -3) . ':' .
                substr($time, 6)
                . ' นาฬิกา';
                break;
            case 's':
                echo substr($time, 0, -6) . ':' .
                substr($time, 3, -3) . ':' .
                substr($time, 6)
                . ' น.';
                break;
            case 'n':
                echo substr($time, 0, -6) . ':' .
                substr($time, 3, -3) . ':' .
                substr($time, 6);
                break;

            default:
                echo '-';
                break;
        }
    }

    public function ShortDate($date) {

        $dtmp = explode("-", $date);
        if (count($dtmp) == 3) {
            $cdate = checkdate($dtmp[1], $dtmp[2], $dtmp[0]);
            if ($cdate) {
                return intval($dtmp[2]) . '/' . intval($dtmp[1]) . '/' . ($dtmp[0] + 543);
            } else {
                return 'รูปแบบวันที่ไม่ถูกต้อง';
            }
        } else {
            return '-';
        }
    }

}

//
//           switch ($type) {
//                    case 'f':
//                        echo (int) substr($date, 8) . ' ' .
//                        $this->monthFull[substr($date, 5, -3)] . ' พ.ศ. ' . ((int) substr($date, 0, -6) + 543);
//                        break;
//                    case 's':
//                        echo (int) substr($date, 8) . ' ' .
//                        $this->monthShrot[substr($date, 5, -3)] . ' ' . ((int) substr($date, 0, -6) + 543);
//                        break;
//                    case 'n':
//                        echo (int) substr($date, 8) . '/' .
//                        $this->monthNumber[substr($date, 5, -3)] . '/' . ((int) substr($date, 0, -6) + 543);
//                        break;
//                    default:
//                        echo '-';
//                        break;
//                }