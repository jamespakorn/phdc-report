<?php
namespace app\controllers;

use Yii;
use app\models\ReportForm;

class CheckStudentController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $model = new ReportForm;
        $model->eyear=2017;
        $model->school='%';
        

                 //POST
        if ($model->load(Yii::$app->request->post())) {

            $post = (object) Yii::$app->request->post('ReportForm');
            $model->eyear=$post->eyear;
            $post->school==null? $model->school='%':$model->school=$post->school;

        }


        $conn = Yii::$app->db;

        $sql = "SELECT s.hospcode,h.hosname,
        COUNT(IF(class='00',concat(s.hospcode,'-',s.pid),null)) as '00',
        COUNT(IF(class='01',concat(s.hospcode,'-',s.pid),null)) as '01',
        COUNT(IF(class='02',concat(s.hospcode,'-',s.pid),null)) as '02',
        COUNT(IF(class='03',concat(s.hospcode,'-',s.pid),null)) as '03',
        COUNT(IF(class='11',concat(s.hospcode,'-',s.pid),null)) as '11',
        COUNT(IF(class='12',concat(s.hospcode,'-',s.pid),null)) as '12',
        COUNT(IF(class='13',concat(s.hospcode,'-',s.pid),null)) as '13',
        COUNT(IF(class='21',concat(s.hospcode,'-',s.pid),null)) as '21',
        COUNT(IF(class='22',concat(s.hospcode,'-',s.pid),null)) as '22',
        COUNT(IF(class='23',concat(s.hospcode,'-',s.pid),null)) as '23',
        COUNT(IF(class='31',concat(s.hospcode,'-',s.pid),null)) as '31',
        COUNT(IF(class='32',concat(s.hospcode,'-',s.pid),null)) as '32',
        COUNT(IF(class='33',concat(s.hospcode,'-',s.pid),null)) as '33',
        COUNT(IF(class='41',concat(s.hospcode,'-',s.pid),null)) as '41',
        COUNT(IF(class='42',concat(s.hospcode,'-',s.pid),null)) as '42',
        COUNT(IF(class='43',concat(s.hospcode,'-',s.pid),null)) as '43',
        COUNT(concat(s.hospcode,'-',s.pid)) as total

        FROM student_r11 s
        INNER JOIN t_person_db p on s.pid=p.pid and s.hospcode=p.hospcode
        LEFT JOIN chospital h ON h.hoscode=s.hospcode
        WHERE educationyear=:educationyear
        AND concat(s.hospcode,'-',s.schoolcode) LIKE :school
        GROUP BY s.hospcode
        ORDER BY s.hospcode";

        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':educationyear', (int)$model->eyear+543);
        $cmd->bindParam(':school', $model->school);
        $students = $cmd->queryAll();

        return $this->render('index', [
            'model' => $model,
            'students' => $students]);
    }

}
