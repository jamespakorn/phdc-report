<?php
namespace app\controllers;

use app\models\Ctambon;
use app\models\ReportForm;
use Yii;
use yii\data\ArrayDataProvider;
use yii\helpers\Json;

class ReportAnc12Controller extends \yii\web\Controller
{
    public function actionIndex()
    {
        $model = new ReportForm;
        $model->byear = date('Y')-5;
        $model->start_d = ((int) $model->byear - 1) . '-10-01';
        $model->end_d = $model->byear . '-09-30';
        $model->changwatcode = Yii::$app->params['provinceCode'];
        $model->ampurcode = null;
        $reportType = 1;
        $ampurStyle = null;
        $tambonCode = null;
        $model->ageMin = 1;
        $model->ageMax = 19;

        //POST
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            $post = (object) Yii::$app->request->post('ReportForm');
            $model->byear = $post->byear;
            $model->changwatcode = $post->changwatcode;
            $model->ampurcode = $post->ampurcode;
            $model->start_d = ((int) $model->byear - 1) . '-10-01';
            $model->end_d = $model->byear . '-09-30';
            $model->mastercup = $post->mastercup;
            // $model->ageMin = $post->ageMin;
            // $model->ageMax = $post->ageMax;

            //check ampurstyle
            if (isset($post->ampurstyle)) {
                $ampurStyle = $post->ampurstyle;
                $model->ampurstyle = $post->ampurstyle;

                //check tamboncode
                if (isset($post->tamboncode)) {
                    $tambonCode = $post->tamboncode;
                    $model->tamboncode = $post->tamboncode;
                }

            }

            if ($post->mastercup != '') {
                $reportType = 5;
            } else if ($post->ampurcode !== '') {
                if ($ampurStyle == null | $ampurStyle == '01') {
                    $reportType = 2;
                } else if ($ampurStyle == '02') {
                    //Tambon
                    if ($tambonCode == null) {
                        $reportType = 3;
                    } else {
                        $reportType = 4;
                    }
                }
            }

        }

        if ($reportType == 1) {
            $result = (object) $this->getSQLChangwat($model);
        } else if ($reportType == 2) {
            $result = (object) $this->getSQLAmpurHospital($model);
        } else if ($reportType == 3) {
            $result = (object) $this->getSQLAmpurTambon($model);
        } else if ($reportType == 4) {
            $result = (object) $this->getSQLTambonVillage($model);
        } else if ($reportType == 5) {
            $result = (object) $this->getSQLMastercup($model);
        }

        $dataProvider = new ArrayDataProvider([
            'allModels' => $result->data,
            'pagination' => false,
        ]);

        return $this->render('index', [
            'model' => $model,
            'sql' => $result->sql,
            'sqlParams' => implode(', ', $result->cmd->params),
            'reportType' => $reportType,
            'dataProvider' => $dataProvider]);
    }

    public function getReportTypeName($type)
    {
        if ($type === 1) {
            return 'อำเภอ';
        } else if ($type === 2) {
            return 'หน่วยบริการ';
        } else if ($type === 3) {
            return 'ตำบล';
        } else if ($type === 4) {
            return 'หมู่บ้าน';
        } else if ($type === 5) {
            return 'เครือข่ายบริการสุขภาพ';
        }
    }

    public function actionDepdropampurstyle()
    {
        $out = [];
        if (Yii::$app->request->post('depdrop_parents')) {
            $parents = Yii::$app->request->post('depdrop_parents');
            if ($parents != null) {
                if ($parents[0] !== '') {
                    $out = [
                        ['id' => '01', 'name' => 'หน่วยบริการ'],
                        ['id' => '02', 'name' => 'ตำบล/แขวง'],
                    ];
                    return Json::encode(['output' => $out, 'selected' => '']);
                } else {
                    return Json::encode(['output' => $out, 'selected' => '']);
                }

            }
        }

        return Json::encode(['output' => $out, 'selected' => '']);
    }

    public function Ampurstyledata($id)
    {

        if ($id == '01') {
            return ['01' => 'หน่วยบริการ'];
        } else if ($id == '02') {
            return ['02' => 'ตำบล/แขวง'];
        } else {
            return [];
        }
    }

    public function actionDepdroptambon()
    {
        $out = [];
        if (Yii::$app->request->post('depdrop_parents')) {
            $ids = Yii::$app->request->post('depdrop_parents');
            $ampurcode = empty($ids[0]) ? null : $ids[0];
            $ampurstyle = empty($ids[1]) ? null : $ids[1];
            if ($ampurcode != null && $ampurstyle == '02') {
                $out = Ctambon::find()
                    ->where(['ampurcode' => $ampurcode])
                    ->andWhere(['flag_status' => 0])
                    ->select(['tamboncodefull AS id', 'tambonname AS name'])->asArray()->all();
                return Json::encode(['output' => $out, 'selected' => '']);
            } else {
                return Json::encode(['output' => $out, 'selected' => '']);
            }

        }
        return Json::encode(['output' => '', 'selected' => '']);
    }

    public function getSQLChangwat($model)
    {
        $conn = Yii::$app->db;

        $sql = "SELECT amp.ampurcodefull as areacode,amp.ampurname as areaname,p.check_hosp,p.check_vhid
         ,DATE_FORMAT(now(),'%Y%m%d%H%i') as d_com

                -- All
        ,COUNT(DISTINCT CONCAT(l.cid,'-',l.bdate)) target
        ,COUNT(DISTINCT IF( a.ga <=12, CONCAT(a.cid,'-',a.gravida),NULL)) result
        ,COUNT(DISTINCT IF( a.ga <=12, CONCAT(a.cid,'-',a.gravida),NULL))/COUNT(DISTINCT CONCAT(l.cid,'-',a.gravida))*100 percent
        -- T1
        ,COUNT(DISTINCT IF(DATE_FORMAT(l.bdate,'%m') IN(10,11,12), CONCAT(l.cid,'-',l.bdate) ,NULL)) targetq1
        ,COUNT(DISTINCT IF(DATE_FORMAT(l.bdate,'%m') IN(10,11,12) AND a.ga <=12, CONCAT(a.cid,'-',a.gravida),NULL)) resultq1
        ,COUNT(DISTINCT IF(DATE_FORMAT(l.bdate,'%m') IN(10,11,12) AND a.ga <=12, CONCAT(a.cid,'-',a.gravida),NULL))
        /COUNT(DISTINCT IF(DATE_FORMAT(l.bdate,'%m') IN(10,11,12), CONCAT(l.cid,'-',a.gravida) ,NULL))*100 percentq1
        -- T2
        ,COUNT(DISTINCT IF(DATE_FORMAT(l.bdate,'%m') IN(1,2,3), CONCAT(l.cid,'-',l.bdate) ,NULL)) targetq2
        ,COUNT(DISTINCT IF(DATE_FORMAT(l.bdate,'%m') IN(1,2,3) AND a.ga <=12, CONCAT(a.cid,'-',a.gravida),NULL)) resultq2
        ,COUNT(DISTINCT IF(DATE_FORMAT(l.bdate,'%m') IN(1,2,3) AND a.ga <=12, CONCAT(a.cid,'-',a.gravida),NULL))
        /COUNT(DISTINCT IF(DATE_FORMAT(l.bdate,'%m') IN(1,2,3), CONCAT(l.cid,'-',a.gravida) ,NULL))*100 percentq2
        -- T3
        ,COUNT(DISTINCT IF(DATE_FORMAT(l.bdate,'%m') IN(4,5,6), CONCAT(l.cid,'-',l.bdate) ,NULL)) targetq3
        ,COUNT(DISTINCT IF(DATE_FORMAT(l.bdate,'%m') IN(4,5,6) AND a.ga <=12, CONCAT(a.cid,'-',a.gravida),NULL)) resultq3
        ,COUNT(DISTINCT IF(DATE_FORMAT(l.bdate,'%m') IN(4,5,6) AND a.ga <=12, CONCAT(a.cid,'-',a.gravida),NULL))
        /COUNT(DISTINCT IF(DATE_FORMAT(l.bdate,'%m') IN(4,5,6), CONCAT(l.cid,'-',a.gravida) ,NULL))*100 percentq3
        -- T4
        ,COUNT(DISTINCT IF(DATE_FORMAT(l.bdate,'%m') IN(7,8,9), CONCAT(l.cid,'-',l.bdate) ,NULL)) targetq4
        ,COUNT(DISTINCT IF(DATE_FORMAT(l.bdate,'%m') IN(7,8,9) AND a.ga <=12, CONCAT(a.cid,'-',a.gravida),NULL)) resultq4
        ,COUNT(DISTINCT IF(DATE_FORMAT(l.bdate,'%m') IN(7,8,9) AND a.ga <=12, CONCAT(a.cid,'-',a.gravida),NULL))
        /COUNT(DISTINCT IF(DATE_FORMAT(l.bdate,'%m') IN(7,8,9), CONCAT(l.cid,'-',a.gravida) ,NULL))*100 percentq4

        FROM tmp_labor l
        INNER JOIN t_person_cid p ON l.cid=p.cid
        INNER JOIN chospital h ON p.check_hosp=h.hoscode
        LEFT JOIN tmp_anc a ON l.cid=a.cid AND l.gravida=a.gravida
         LEFT JOIN campur amp on amp.ampurcodefull=left(p.check_vhid,4)
         WHERE l.BDATE BETWEEN :start_d AND :end_d
         AND p.check_typearea in(1,3) AND p.discharge IN(9)
         AND p.nation in(99) AND h.provcode=:changwatcode
         AND p.age_y BETWEEN :min and :max
         GROUP BY areacode
         WITH ROLLUP";

        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':start_d', $model->start_d);
        $cmd->bindValue(':end_d', $model->end_d);
        $cmd->bindValue(':changwatcode', $model->changwatcode);
        $cmd->bindValue(':min', $model->ageMin);
        $cmd->bindValue(':max', $model->ageMax);
        $data = $cmd->queryAll();

        return ['sql' => $sql, 'cmd' => $cmd, 'data' => $data];
    }

    public function getSQLAmpurHospital($model)
    {
        $conn = Yii::$app->db;

        $sql = "SELECT h.hoscode as areacode,h.hosname as areaname,p.check_hosp,p.check_vhid
        ,DATE_FORMAT(now(),'%Y%m%d%H%i') as d_com
        -- All
        ,COUNT(DISTINCT CONCAT(l.cid,'-',l.bdate)) target
        ,COUNT(DISTINCT IF( a.ga <=12, CONCAT(a.cid,'-',a.gravida),NULL)) result
        ,COUNT(DISTINCT IF( a.ga <=12, CONCAT(a.cid,'-',a.gravida),NULL))/COUNT(DISTINCT CONCAT(l.cid,'-',a.gravida))*100 percent
        -- T1
        ,COUNT(DISTINCT IF(DATE_FORMAT(l.bdate,'%m') IN(10,11,12), CONCAT(l.cid,'-',l.bdate) ,NULL)) targetq1
        ,COUNT(DISTINCT IF(DATE_FORMAT(l.bdate,'%m') IN(10,11,12) AND a.ga <=12, CONCAT(a.cid,'-',a.gravida),NULL)) resultq1
        ,COUNT(DISTINCT IF(DATE_FORMAT(l.bdate,'%m') IN(10,11,12) AND a.ga <=12, CONCAT(a.cid,'-',a.gravida),NULL))
        /COUNT(DISTINCT IF(DATE_FORMAT(l.bdate,'%m') IN(10,11,12), CONCAT(l.cid,'-',a.gravida) ,NULL))*100 percentq1
        -- T2
        ,COUNT(DISTINCT IF(DATE_FORMAT(l.bdate,'%m') IN(1,2,3), CONCAT(l.cid,'-',l.bdate) ,NULL)) targetq2
        ,COUNT(DISTINCT IF(DATE_FORMAT(l.bdate,'%m') IN(1,2,3) AND a.ga <=12, CONCAT(a.cid,'-',a.gravida),NULL)) resultq2
        ,COUNT(DISTINCT IF(DATE_FORMAT(l.bdate,'%m') IN(1,2,3) AND a.ga <=12, CONCAT(a.cid,'-',a.gravida),NULL))
        /COUNT(DISTINCT IF(DATE_FORMAT(l.bdate,'%m') IN(1,2,3), CONCAT(l.cid,'-',a.gravida) ,NULL))*100 percentq2
        -- T3
        ,COUNT(DISTINCT IF(DATE_FORMAT(l.bdate,'%m') IN(4,5,6), CONCAT(l.cid,'-',l.bdate) ,NULL)) targetq3
        ,COUNT(DISTINCT IF(DATE_FORMAT(l.bdate,'%m') IN(4,5,6) AND a.ga <=12, CONCAT(a.cid,'-',a.gravida),NULL)) resultq3
        ,COUNT(DISTINCT IF(DATE_FORMAT(l.bdate,'%m') IN(4,5,6) AND a.ga <=12, CONCAT(a.cid,'-',a.gravida),NULL))
        /COUNT(DISTINCT IF(DATE_FORMAT(l.bdate,'%m') IN(4,5,6), CONCAT(l.cid,'-',a.gravida) ,NULL))*100 percentq3
        -- T4
        ,COUNT(DISTINCT IF(DATE_FORMAT(l.bdate,'%m') IN(7,8,9), CONCAT(l.cid,'-',l.bdate) ,NULL)) targetq4
        ,COUNT(DISTINCT IF(DATE_FORMAT(l.bdate,'%m') IN(7,8,9) AND a.ga <=12, CONCAT(a.cid,'-',a.gravida),NULL)) resultq4
        ,COUNT(DISTINCT IF(DATE_FORMAT(l.bdate,'%m') IN(7,8,9) AND a.ga <=12, CONCAT(a.cid,'-',a.gravida),NULL))
        /COUNT(DISTINCT IF(DATE_FORMAT(l.bdate,'%m') IN(7,8,9), CONCAT(l.cid,'-',a.gravida) ,NULL))*100 percentq4

        FROM tmp_labor l
        INNER JOIN t_person_cid p ON l.cid=p.cid
        INNER JOIN chospital h ON p.check_hosp=h.hoscode
        LEFT JOIN tmp_anc a ON l.cid=a.cid AND l.gravida=a.gravida
        WHERE l.BDATE BETWEEN :start_d AND :end_d
        AND p.check_typearea in(1,3) AND p.discharge IN(9)
        AND p.nation in(99) AND h.provcode=:changwatcode
        AND p.age_y BETWEEN :min and :max
        AND h.hdc_regist=1 AND CONCAT(h.provcode,h.distcode)=:ampurcode
        GROUP BY areacode
        WITH ROLLUP";

        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':start_d', $model->start_d);
        $cmd->bindValue(':end_d', $model->end_d);
        $cmd->bindValue(':changwatcode', $model->changwatcode);
        $cmd->bindValue(':min', $model->ageMin);
        $cmd->bindValue(':max', $model->ageMax);
        $cmd->bindValue(':ampurcode', $model->ampurcode);
        $data = $cmd->queryAll();

        return ['sql' => $sql, 'cmd' => $cmd, 'data' => $data];
    }

    public function getSQLAmpurTambon($model)
    {
        $conn = Yii::$app->db;

        $sql = "SELECT t.tamboncodefull as areacode,t.tambonname as areaname,p.check_hosp,p.check_vhid
        ,DATE_FORMAT(now(),'%Y%m%d%H%i') as d_com
        -- All
        ,COUNT(DISTINCT CONCAT(l.cid,'-',l.bdate)) target
        ,COUNT(DISTINCT IF( a.ga <=12, CONCAT(a.cid,'-',a.gravida),NULL)) result
        ,COUNT(DISTINCT IF( a.ga <=12, CONCAT(a.cid,'-',a.gravida),NULL))/COUNT(DISTINCT CONCAT(l.cid,'-',a.gravida))*100 percent
        -- T1
        ,COUNT(DISTINCT IF(DATE_FORMAT(l.bdate,'%m') IN(10,11,12), CONCAT(l.cid,'-',l.bdate) ,NULL)) targetq1
        ,COUNT(DISTINCT IF(DATE_FORMAT(l.bdate,'%m') IN(10,11,12) AND a.ga <=12, CONCAT(a.cid,'-',a.gravida),NULL)) resultq1
        ,COUNT(DISTINCT IF(DATE_FORMAT(l.bdate,'%m') IN(10,11,12) AND a.ga <=12, CONCAT(a.cid,'-',a.gravida),NULL))
        /COUNT(DISTINCT IF(DATE_FORMAT(l.bdate,'%m') IN(10,11,12), CONCAT(l.cid,'-',a.gravida) ,NULL))*100 percentq1
        -- T2
        ,COUNT(DISTINCT IF(DATE_FORMAT(l.bdate,'%m') IN(1,2,3), CONCAT(l.cid,'-',l.bdate) ,NULL)) targetq2
        ,COUNT(DISTINCT IF(DATE_FORMAT(l.bdate,'%m') IN(1,2,3) AND a.ga <=12, CONCAT(a.cid,'-',a.gravida),NULL)) resultq2
        ,COUNT(DISTINCT IF(DATE_FORMAT(l.bdate,'%m') IN(1,2,3) AND a.ga <=12, CONCAT(a.cid,'-',a.gravida),NULL))
        /COUNT(DISTINCT IF(DATE_FORMAT(l.bdate,'%m') IN(1,2,3), CONCAT(l.cid,'-',a.gravida) ,NULL))*100 percentq2
        -- T3
        ,COUNT(DISTINCT IF(DATE_FORMAT(l.bdate,'%m') IN(4,5,6), CONCAT(l.cid,'-',l.bdate) ,NULL)) targetq3
        ,COUNT(DISTINCT IF(DATE_FORMAT(l.bdate,'%m') IN(4,5,6) AND a.ga <=12, CONCAT(a.cid,'-',a.gravida),NULL)) resultq3
        ,COUNT(DISTINCT IF(DATE_FORMAT(l.bdate,'%m') IN(4,5,6) AND a.ga <=12, CONCAT(a.cid,'-',a.gravida),NULL))
        /COUNT(DISTINCT IF(DATE_FORMAT(l.bdate,'%m') IN(4,5,6), CONCAT(l.cid,'-',a.gravida) ,NULL))*100 percentq3
        -- T4
        ,COUNT(DISTINCT IF(DATE_FORMAT(l.bdate,'%m') IN(7,8,9), CONCAT(l.cid,'-',l.bdate) ,NULL)) targetq4
        ,COUNT(DISTINCT IF(DATE_FORMAT(l.bdate,'%m') IN(7,8,9) AND a.ga <=12, CONCAT(a.cid,'-',a.gravida),NULL)) resultq4
        ,COUNT(DISTINCT IF(DATE_FORMAT(l.bdate,'%m') IN(7,8,9) AND a.ga <=12, CONCAT(a.cid,'-',a.gravida),NULL))
        /COUNT(DISTINCT IF(DATE_FORMAT(l.bdate,'%m') IN(7,8,9), CONCAT(l.cid,'-',a.gravida) ,NULL))*100 percentq4

        FROM tmp_labor l
        INNER JOIN t_person_cid p ON l.cid=p.cid
        INNER JOIN chospital h ON p.check_hosp=h.hoscode
        LEFT JOIN tmp_anc a ON l.cid=a.cid AND l.gravida=a.gravida
        LEFT JOIN ctambon t on t.tamboncodefull=left(p.check_vhid,6)
        WHERE l.BDATE BETWEEN :start_d AND :end_d
        AND p.check_typearea in(1,3) AND p.discharge IN(9)
        AND p.nation in(99) AND h.provcode=:changwatcode
        AND p.age_y BETWEEN :min and :max
        AND t.ampurcode=:ampurcode
        GROUP BY areacode
        WITH ROLLUP";

        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':start_d', $model->start_d);
        $cmd->bindValue(':end_d', $model->end_d);
        $cmd->bindValue(':changwatcode', $model->changwatcode);
        $cmd->bindValue(':min', $model->ageMin);
        $cmd->bindValue(':max', $model->ageMax);
        $cmd->bindValue(':ampurcode', $model->ampurcode);
        $data = $cmd->queryAll();

        return ['sql' => $sql, 'cmd' => $cmd, 'data' => $data];
    }

    public function getSQLTambonVillage($model)
    {
        $conn = Yii::$app->db;

        $sql = "SELECT v.villagecodefull as areacode,concat(' ',v.villagecode,' ',v.villagename) as areaname,p.check_hosp,p.check_vhid
        ,DATE_FORMAT(now(),'%Y%m%d%H%i') as d_com
        -- All
        ,COUNT(DISTINCT CONCAT(l.cid,'-',l.bdate)) target
        ,COUNT(DISTINCT IF( a.ga <=12, CONCAT(a.cid,'-',a.gravida),NULL)) result
        ,COUNT(DISTINCT IF( a.ga <=12, CONCAT(a.cid,'-',a.gravida),NULL))/COUNT(DISTINCT CONCAT(l.cid,'-',a.gravida))*100 percent
        -- T1
        ,COUNT(DISTINCT IF(DATE_FORMAT(l.bdate,'%m') IN(10,11,12), CONCAT(l.cid,'-',l.bdate) ,NULL)) targetq1
        ,COUNT(DISTINCT IF(DATE_FORMAT(l.bdate,'%m') IN(10,11,12) AND a.ga <=12, CONCAT(a.cid,'-',a.gravida),NULL)) resultq1
        ,COUNT(DISTINCT IF(DATE_FORMAT(l.bdate,'%m') IN(10,11,12) AND a.ga <=12, CONCAT(a.cid,'-',a.gravida),NULL))
        /COUNT(DISTINCT IF(DATE_FORMAT(l.bdate,'%m') IN(10,11,12), CONCAT(l.cid,'-',a.gravida) ,NULL))*100 percentq1
        -- T2
        ,COUNT(DISTINCT IF(DATE_FORMAT(l.bdate,'%m') IN(1,2,3), CONCAT(l.cid,'-',l.bdate) ,NULL)) targetq2
        ,COUNT(DISTINCT IF(DATE_FORMAT(l.bdate,'%m') IN(1,2,3) AND a.ga <=12, CONCAT(a.cid,'-',a.gravida),NULL)) resultq2
        ,COUNT(DISTINCT IF(DATE_FORMAT(l.bdate,'%m') IN(1,2,3) AND a.ga <=12, CONCAT(a.cid,'-',a.gravida),NULL))
        /COUNT(DISTINCT IF(DATE_FORMAT(l.bdate,'%m') IN(1,2,3), CONCAT(l.cid,'-',a.gravida) ,NULL))*100 percentq2
        -- T3
        ,COUNT(DISTINCT IF(DATE_FORMAT(l.bdate,'%m') IN(4,5,6), CONCAT(l.cid,'-',l.bdate) ,NULL)) targetq3
        ,COUNT(DISTINCT IF(DATE_FORMAT(l.bdate,'%m') IN(4,5,6) AND a.ga <=12, CONCAT(a.cid,'-',a.gravida),NULL)) resultq3
        ,COUNT(DISTINCT IF(DATE_FORMAT(l.bdate,'%m') IN(4,5,6) AND a.ga <=12, CONCAT(a.cid,'-',a.gravida),NULL))
        /COUNT(DISTINCT IF(DATE_FORMAT(l.bdate,'%m') IN(4,5,6), CONCAT(l.cid,'-',a.gravida) ,NULL))*100 percentq3
        -- T4
        ,COUNT(DISTINCT IF(DATE_FORMAT(l.bdate,'%m') IN(7,8,9), CONCAT(l.cid,'-',l.bdate) ,NULL)) targetq4
        ,COUNT(DISTINCT IF(DATE_FORMAT(l.bdate,'%m') IN(7,8,9) AND a.ga <=12, CONCAT(a.cid,'-',a.gravida),NULL)) resultq4
        ,COUNT(DISTINCT IF(DATE_FORMAT(l.bdate,'%m') IN(7,8,9) AND a.ga <=12, CONCAT(a.cid,'-',a.gravida),NULL))
        /COUNT(DISTINCT IF(DATE_FORMAT(l.bdate,'%m') IN(7,8,9), CONCAT(l.cid,'-',a.gravida) ,NULL))*100 percentq4

        FROM tmp_labor l
        INNER JOIN t_person_cid p ON l.cid=p.cid
        INNER JOIN chospital h ON p.check_hosp=h.hoscode
        LEFT JOIN tmp_anc a ON l.cid=a.cid AND l.gravida=a.gravida
        LEFT JOIN cvillage v on v.villagecodefull=left(p.check_vhid,8)
        WHERE l.BDATE BETWEEN :start_d AND :end_d
        AND p.check_typearea in(1,3) AND p.discharge IN(9)
        AND p.nation in(99) AND h.provcode=:changwatcode
        AND p.age_y BETWEEN :min and :max
        AND v.tamboncode=:tamboncode
        GROUP BY areacode
        WITH ROLLUP";

        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':start_d', $model->start_d);
        $cmd->bindValue(':end_d', $model->end_d);
        $cmd->bindValue(':changwatcode', $model->changwatcode);
        $cmd->bindValue(':min', $model->ageMin);
        $cmd->bindValue(':max', $model->ageMax);
        $cmd->bindValue(':tamboncode', $model->tamboncode);
        $data = $cmd->queryAll();

        return ['sql' => $sql, 'cmd' => $cmd, 'data' => $data];
    }

    public function getSQLMastercup($model)
    {
        $conn = Yii::$app->db;

        $sql = "SELECT h.hoscode as areacode,h.hosname as areaname,p.check_hosp,p.check_vhid
        ,DATE_FORMAT(now(),'%Y%m%d%H%i') as d_com
        -- All
        ,COUNT(DISTINCT CONCAT(l.cid,'-',l.bdate)) target
        ,COUNT(DISTINCT IF( a.ga <=12, CONCAT(a.cid,'-',a.gravida),NULL)) result
        ,COUNT(DISTINCT IF( a.ga <=12, CONCAT(a.cid,'-',a.gravida),NULL))/COUNT(DISTINCT CONCAT(l.cid,'-',a.gravida))*100 percent
        -- T1
        ,COUNT(DISTINCT IF(DATE_FORMAT(l.bdate,'%m') IN(10,11,12), CONCAT(l.cid,'-',l.bdate) ,NULL)) targetq1
        ,COUNT(DISTINCT IF(DATE_FORMAT(l.bdate,'%m') IN(10,11,12) AND a.ga <=12, CONCAT(a.cid,'-',a.gravida),NULL)) resultq1
        ,COUNT(DISTINCT IF(DATE_FORMAT(l.bdate,'%m') IN(10,11,12) AND a.ga <=12, CONCAT(a.cid,'-',a.gravida),NULL))
        /COUNT(DISTINCT IF(DATE_FORMAT(l.bdate,'%m') IN(10,11,12), CONCAT(l.cid,'-',a.gravida) ,NULL))*100 percentq1
        -- T2
        ,COUNT(DISTINCT IF(DATE_FORMAT(l.bdate,'%m') IN(1,2,3), CONCAT(l.cid,'-',l.bdate) ,NULL)) targetq2
        ,COUNT(DISTINCT IF(DATE_FORMAT(l.bdate,'%m') IN(1,2,3) AND a.ga <=12, CONCAT(a.cid,'-',a.gravida),NULL)) resultq2
        ,COUNT(DISTINCT IF(DATE_FORMAT(l.bdate,'%m') IN(1,2,3) AND a.ga <=12, CONCAT(a.cid,'-',a.gravida),NULL))
        /COUNT(DISTINCT IF(DATE_FORMAT(l.bdate,'%m') IN(1,2,3), CONCAT(l.cid,'-',a.gravida) ,NULL))*100 percentq2
        -- T3
        ,COUNT(DISTINCT IF(DATE_FORMAT(l.bdate,'%m') IN(4,5,6), CONCAT(l.cid,'-',l.bdate) ,NULL)) targetq3
        ,COUNT(DISTINCT IF(DATE_FORMAT(l.bdate,'%m') IN(4,5,6) AND a.ga <=12, CONCAT(a.cid,'-',a.gravida),NULL)) resultq3
        ,COUNT(DISTINCT IF(DATE_FORMAT(l.bdate,'%m') IN(4,5,6) AND a.ga <=12, CONCAT(a.cid,'-',a.gravida),NULL))
        /COUNT(DISTINCT IF(DATE_FORMAT(l.bdate,'%m') IN(4,5,6), CONCAT(l.cid,'-',a.gravida) ,NULL))*100 percentq3
        -- T4
        ,COUNT(DISTINCT IF(DATE_FORMAT(l.bdate,'%m') IN(7,8,9), CONCAT(l.cid,'-',l.bdate) ,NULL)) targetq4
        ,COUNT(DISTINCT IF(DATE_FORMAT(l.bdate,'%m') IN(7,8,9) AND a.ga <=12, CONCAT(a.cid,'-',a.gravida),NULL)) resultq4
        ,COUNT(DISTINCT IF(DATE_FORMAT(l.bdate,'%m') IN(7,8,9) AND a.ga <=12, CONCAT(a.cid,'-',a.gravida),NULL))
        /COUNT(DISTINCT IF(DATE_FORMAT(l.bdate,'%m') IN(7,8,9), CONCAT(l.cid,'-',a.gravida) ,NULL))*100 percentq4

        FROM tmp_labor l
        INNER JOIN t_person_cid p ON l.cid=p.cid
        INNER JOIN chospital h ON p.check_hosp=h.hoscode
        LEFT JOIN tmp_anc a ON l.cid=a.cid AND l.gravida=a.gravida
        INNER JOIN cmastercup c ON h.hoscode = c.hsub
        WHERE l.BDATE BETWEEN :start_d AND :end_d
        AND p.check_typearea in(1,3) AND p.discharge IN(9)
        AND p.nation in(99) AND h.provcode=:changwatcode
        AND p.age_y BETWEEN :min and :max
        AND c.hmain=:mastercup
        GROUP BY areacode
        WITH ROLLUP";

        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':start_d', $model->start_d);
        $cmd->bindValue(':end_d', $model->end_d);
        $cmd->bindValue(':changwatcode', $model->changwatcode);
        $cmd->bindValue(':min', $model->ageMin);
        $cmd->bindValue(':max', $model->ageMax);
        $cmd->bindValue(':mastercup', $model->mastercup);
        $data = $cmd->queryAll();

        return ['sql' => $sql, 'cmd' => $cmd, 'data' => $data];
    }

}
