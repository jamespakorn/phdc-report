<?php
namespace app\controllers;

use app\models\Ctambon;
use app\models\Cyear;
use app\models\ReportForm;
use Yii;
use yii\data\ArrayDataProvider;
use yii\helpers\Json;

class ReportGoodHeightController extends \yii\web\Controller
{
    public $class = [
        '11' => 'ป.1', '12' => 'ป.2', '13' => 'ป.3',
        '21' => 'ป.4', '22' => 'ป.5', '23' => 'ป.6',
        '31' => 'ม.1', '32' => 'ม.2', '33' => 'ม.3',
        '41' => 'ม.4', '42' => 'ม.5', '43' => 'ม.6',

    ];

    public function actionIndex()
    {
        $model = new ReportForm;
        $model->byear = date('Y')-5;
        $model->start_d = ((int) $model->byear) . '-05-01';
        $model->end_d = ((int) $model->byear+1) . '-03-31';
        $model->changwatcode = Yii::$app->params['provinceCode'];
        $model->ampurcode = null;
        $reportType = 1;
        $ampurStyle = null;
        $tambonCode = null;
        $model->ageMin = 30;
        $model->ageMax = 44;
        $model->class = 11;

        //POST
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            $post = (object) Yii::$app->request->post('ReportForm');
            $model->byear = $post->byear;
            $model->changwatcode = $post->changwatcode;
            $model->ampurcode = $post->ampurcode;
            $model->start_d = ((int) $model->byear) . '-05-01';
            $model->end_d = ((int) $model->byear+1) . '-03-31';
            $model->mastercup = $post->mastercup;
            $model->class = $post->class;

           
            //check ampurstyle
            if (isset($post->ampurstyle)) {
                $ampurStyle = $post->ampurstyle;
                $model->ampurstyle = $post->ampurstyle;

                //check tamboncode
                if (isset($post->tamboncode)) {
                    $tambonCode = $post->tamboncode;
                    $model->tamboncode = $post->tamboncode;
                }

            }

            if ($post->mastercup != '') {
                $reportType = 5;
            } else if ($post->ampurcode !== '') {
                if ($ampurStyle == null | $ampurStyle == '01') {
                    $reportType = 2;
                } else if ($ampurStyle == '02') {
                    //Tambon
                    if ($tambonCode == null) {
                        $reportType = 3;
                    } else {
                        $reportType = 4;
                    }
                } else if ($ampurStyle == '03') {
                    $reportType = 6;
                }
            }

        }

        if ($reportType == 1) {
            $result = (object) $this->getSQLChangwat($model);
        } else if ($reportType == 2) {
            $result = (object) $this->getSQLAmpurHospital($model);
        } else if ($reportType == 3) {
            $result = (object) $this->getSQLAmpurTambon($model);
        } else if ($reportType == 4) {
            $result = (object) $this->getSQLTambonVillage($model);
        } else if ($reportType == 5) {
            $result = (object) $this->getSQLMastercup($model);
        } else if ($reportType == 6) {
            $result = (object) $this->getSQLAmpurSchool($model);
        }

        $dataProvider = new ArrayDataProvider([
            'allModels' => $result->data,
            'pagination' => false,
        ]);

        return $this->render('index', [
            'model' => $model,
            'sql' => $result->sql,
            'sqlParams' => implode(', ', $result->cmd->params),
            'reportType' => $reportType,
            'dataProvider' => $dataProvider]);
    }

    public function getReportTypeName($type)
    {
        if ($type === 1) {
            return 'อำเภอ';
        } else if ($type === 2) {
            return 'หน่วยบริการ';
        } else if ($type === 3) {
            return 'ตำบล';
        } else if ($type === 4) {
            return 'หมู่บ้าน';
        } else if ($type === 5) {
            return 'เครือข่ายบริการสุขภาพ';
        } else if ($type === 6) {
            return 'โรงเรียน';
        }
    }

    public function actionDepdropampurstyle()
    {
        $out = [];
        if (Yii::$app->request->post('depdrop_parents')) {
            $parents = Yii::$app->request->post('depdrop_parents');
            if ($parents != null) {
                if ($parents[0] !== '') {
                    $out = [
                        ['id' => '01', 'name' => 'หน่วยบริการ'],
                        ['id' => '02', 'name' => 'ตำบล/แขวง'],
                        ['id' => '03', 'name' => 'โรงเรียน'],
                    ];
                    return Json::encode(['output' => $out, 'selected' => '']);
                } else {
                    return Json::encode(['output' => $out, 'selected' => '']);
                }

            }
        }

        return Json::encode(['output' => $out, 'selected' => '']);
    }

    public function Ampurstyledata($id)
    {

        if ($id == '01') {
            return ['01' => 'หน่วยบริการ'];
        } else if ($id == '02') {
            return ['02' => 'ตำบล/แขวง'];
        } else if ($id == '03') {
            return ['03' => 'โรงเรียน'];
        } else {
            return [];
        }
    }

    public function actionDepdroptambon()
    {
        $out = [];
        if (Yii::$app->request->post('depdrop_parents')) {
            $ids = Yii::$app->request->post('depdrop_parents');
            $ampurcode = empty($ids[0]) ? null : $ids[0];
            $ampurstyle = empty($ids[1]) ? null : $ids[1];
            if ($ampurcode != null && $ampurstyle == '02') {
                $out = Ctambon::find()
                    ->where(['ampurcode' => $ampurcode])
                    ->andWhere(['flag_status' => 0])
                    ->select(['tamboncodefull AS id', 'tambonname AS name'])->asArray()->all();
                return Json::encode(['output' => $out, 'selected' => '']);
            } else {
                return Json::encode(['output' => $out, 'selected' => '']);
            }

        }
        return Json::encode(['output' => '', 'selected' => '']);
    }

    public function getClass()
    {
        return $this->class;
    }

    public function getClassName($id)
    {
        if (($className = $this->class[$id]) !== null) {
            return $className;
        } else {
            return null;
        }
    }

    public function getSQLChangwat($model)
    {
        $conn = Yii::$app->db;

        $sql = "SELECT x.ampurcodefull as areacode,x.ampurname as areaname,x.check_hosp,x.check_vhid,DATE_FORMAT(now(),'%Y%m%d%H%i') as d_com

        ,COUNT(DISTINCT x.cid) as B0

        ,COUNT(DISTINCT if(x.term=1, CONCAT(x.HOSPCODE,'-',x.PID),NULL)) B1T1
        ,COUNT(DISTINCT if(x.term=2, CONCAT(x.HOSPCODE,'-',x.PID),NULL)) B1T2

        ,COUNT(DISTINCT if(x.term=1 AND nutri2 in(3,4,5) AND nutri3 in(3), CONCAT(x.HOSPCODE,'-',x.PID),NULL)) A1T1
        ,COUNT(DISTINCT if(x.term=2 AND nutri2 in(3,4,5) AND nutri3 in(3), CONCAT(x.HOSPCODE,'-',x.PID),NULL)) A1T2

        ,COUNT(DISTINCT if(x.term=1 AND nutri3 in(1), CONCAT(x.HOSPCODE,'-',x.PID),NULL)) A2T1
        ,COUNT(DISTINCT if(x.term=2 AND nutri3 in(1), CONCAT(x.HOSPCODE,'-',x.PID),NULL)) A2T2

        ,COUNT(DISTINCT if(x.term=1 AND nutri3 IN(5,6,7), CONCAT(x.HOSPCODE,'-',x.PID),NULL)) A3T1
        ,COUNT(DISTINCT if(x.term=2 AND nutri3 IN(5,6,7), CONCAT(x.HOSPCODE,'-',x.PID),NULL)) A3T2

        ,COUNT(DISTINCT if(x.term=1 AND nutri2 in(1), CONCAT(x.HOSPCODE,'-',x.PID),NULL)) A4T1
        ,COUNT(DISTINCT if(x.term=2 AND nutri2 in(1), CONCAT(x.HOSPCODE,'-',x.PID),NULL)) A4T2

        FROM (SELECT n.HOSPCODE,n.PID,p.CID,n.SEQ,n.DATE_SERV,p.check_hosp,p.check_vhid,n.WEIGHT,n.HEIGHT,n.HEADCIRCUM,FOOD,BOTTLE
        ,p.BIRTH,p.SEX,p.NATION, IF(DATE_FORMAT(n.DATE_SERV,'%m') IN(10,11,12,1),2,
        IF(DATE_FORMAT(n.DATE_SERV,'%m') IN(5,6,7),1,0
        )) as term
        ,nutri_cal(TIMESTAMPDIFF(month,birth,date_serv),sex,1,height,weight) as nutri1
        ,nutri_cal(TIMESTAMPDIFF(month,birth,date_serv),sex,2,height,weight) as nutri2
        ,nutri_cal(TIMESTAMPDIFF(month,birth,date_serv),sex,3,height,weight) as nutri3
        ,ap.ampurcodefull,ap.ampurname
        FROM
        t_person_db p
        INNER JOIN chospital h ON h.hoscode=p.hospcode
        INNER JOIN student_r11 s on s.pid=p.pid and s.hospcode=p.hospcode
        LEFT JOIN  (select * from tmp_nutrition where DATE_SERV between :start_d and :end_d
        and WEIGHT between 0.1 and 300 and HEIGHT between 40 and 250 )  n
        ON n.HOSPCODE=p.HOSPCODE AND n.PID=p.PID
        LEFT JOIN school_r11 sc on sc.schoolcode=s.schoolcode and sc.hospcode=s.hospcode
        LEFT JOIN campur ap on ap.ampurcodefull=left(p.check_vhid,4)
        WHERE p.NATION in(99) 
        AND h.provcode=:changwatcode
        AND s.class=:class
        AND s.educationyear=:educationyear) x
        GROUP BY areacode
        WITH ROLLUP";

        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':start_d', $model->start_d);
        $cmd->bindValue(':end_d', $model->end_d);
        $cmd->bindValue(':changwatcode', $model->changwatcode);
        $cmd->bindValue(':class', $model->class);
        $cmd->bindValue(':educationyear', Cyear::GetYearName($model->byear));
        $data = $cmd->queryAll();

        return ['sql' => $sql, 'cmd' => $cmd, 'data' => $data];
    }

    public function getSQLAmpurHospital($model)
    {
        $conn = Yii::$app->db;

        $sql = "SELECT x.hoscode as areacode,x.hosname as areaname,x.check_hosp,x.check_vhid,DATE_FORMAT(now(),'%Y%m%d%H%i') as d_com

        ,COUNT(DISTINCT x.cid) as B0

        ,COUNT(DISTINCT if(x.term=1, CONCAT(x.HOSPCODE,'-',x.PID),NULL)) B1T1
        ,COUNT(DISTINCT if(x.term=2, CONCAT(x.HOSPCODE,'-',x.PID),NULL)) B1T2

        ,COUNT(DISTINCT if(x.term=1 AND nutri2 in(3,4,5) AND nutri3 in(3), CONCAT(x.HOSPCODE,'-',x.PID),NULL)) A1T1
        ,COUNT(DISTINCT if(x.term=2 AND nutri2 in(3,4,5) AND nutri3 in(3), CONCAT(x.HOSPCODE,'-',x.PID),NULL)) A1T2

        ,COUNT(DISTINCT if(x.term=1 AND nutri3 in(1), CONCAT(x.HOSPCODE,'-',x.PID),NULL)) A2T1
        ,COUNT(DISTINCT if(x.term=2 AND nutri3 in(1), CONCAT(x.HOSPCODE,'-',x.PID),NULL)) A2T2

        ,COUNT(DISTINCT if(x.term=1 AND nutri3 IN(5,6,7), CONCAT(x.HOSPCODE,'-',x.PID),NULL)) A3T1
        ,COUNT(DISTINCT if(x.term=2 AND nutri3 IN(5,6,7), CONCAT(x.HOSPCODE,'-',x.PID),NULL)) A3T2

        ,COUNT(DISTINCT if(x.term=1 AND nutri2 in(1), CONCAT(x.HOSPCODE,'-',x.PID),NULL)) A4T1
        ,COUNT(DISTINCT if(x.term=2 AND nutri2 in(1), CONCAT(x.HOSPCODE,'-',x.PID),NULL)) A4T2

        FROM (SELECT n.HOSPCODE,n.PID,p.CID,n.SEQ,n.DATE_SERV,p.check_hosp,p.check_vhid,n.WEIGHT,n.HEIGHT,n.HEADCIRCUM,FOOD,BOTTLE
        ,p.BIRTH,p.SEX,p.NATION, IF(DATE_FORMAT(n.DATE_SERV,'%m') IN(10,11,12,1),2,
        IF(DATE_FORMAT(n.DATE_SERV,'%m') IN(5,6,7),1,0
        )) as term
        ,nutri_cal(TIMESTAMPDIFF(month,birth,date_serv),sex,1,height,weight) as nutri1
        ,nutri_cal(TIMESTAMPDIFF(month,birth,date_serv),sex,2,height,weight) as nutri2
        ,nutri_cal(TIMESTAMPDIFF(month,birth,date_serv),sex,3,height,weight) as nutri3
        ,h.hoscode ,h.hosname 
        FROM
        t_person_db p
        INNER JOIN chospital h ON h.hoscode=p.hospcode
        INNER JOIN student_r11 s on s.pid=p.pid and s.hospcode=p.hospcode
        LEFT JOIN  (select * from tmp_nutrition where DATE_SERV between :start_d and :end_d
        and WEIGHT between 0.1 and 300 and HEIGHT between 40 and 250 )  n
        ON n.HOSPCODE=p.HOSPCODE AND n.PID=p.PID
        LEFT JOIN school_r11 sc on sc.schoolcode=s.schoolcode and sc.hospcode=s.hospcode
        LEFT JOIN campur ap on ap.ampurcodefull=left(p.check_vhid,4)
        WHERE p.NATION in(99) 
        AND h.provcode=:changwatcode
        AND h.hdc_regist=1 AND CONCAT(h.provcode,h.distcode)=:ampurcode
        AND s.class=:class
        AND s.educationyear=:educationyear) x
        GROUP BY areacode
        WITH ROLLUP";

        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':start_d', $model->start_d);
        $cmd->bindValue(':end_d', $model->end_d);
        $cmd->bindValue(':changwatcode', $model->changwatcode);
        $cmd->bindValue(':ampurcode', $model->ampurcode);
        $cmd->bindValue(':class', $model->class);
        $cmd->bindValue(':educationyear', Cyear::GetYearName($model->byear));
        $data = $cmd->queryAll();

        return ['sql' => $sql, 'cmd' => $cmd, 'data' => $data];
    }

    public function getSQLAmpurTambon($model)
    {
        $conn = Yii::$app->db;

        $sql = "SELECT x.tamboncodefull as areacode,x.tambonname as areaname,x.check_hosp,x.check_vhid,DATE_FORMAT(now(),'%Y%m%d%H%i') as d_com

        ,COUNT(DISTINCT x.cid) as B0

        ,COUNT(DISTINCT if(x.term=1, CONCAT(x.HOSPCODE,'-',x.PID),NULL)) B1T1
        ,COUNT(DISTINCT if(x.term=2, CONCAT(x.HOSPCODE,'-',x.PID),NULL)) B1T2

        ,COUNT(DISTINCT if(x.term=1 AND nutri2 in(3,4,5) AND nutri3 in(3), CONCAT(x.HOSPCODE,'-',x.PID),NULL)) A1T1
        ,COUNT(DISTINCT if(x.term=2 AND nutri2 in(3,4,5) AND nutri3 in(3), CONCAT(x.HOSPCODE,'-',x.PID),NULL)) A1T2

        ,COUNT(DISTINCT if(x.term=1 AND nutri3 in(1), CONCAT(x.HOSPCODE,'-',x.PID),NULL)) A2T1
        ,COUNT(DISTINCT if(x.term=2 AND nutri3 in(1), CONCAT(x.HOSPCODE,'-',x.PID),NULL)) A2T2

        ,COUNT(DISTINCT if(x.term=1 AND nutri3 IN(5,6,7), CONCAT(x.HOSPCODE,'-',x.PID),NULL)) A3T1
        ,COUNT(DISTINCT if(x.term=2 AND nutri3 IN(5,6,7), CONCAT(x.HOSPCODE,'-',x.PID),NULL)) A3T2

        ,COUNT(DISTINCT if(x.term=1 AND nutri2 in(1), CONCAT(x.HOSPCODE,'-',x.PID),NULL)) A4T1
        ,COUNT(DISTINCT if(x.term=2 AND nutri2 in(1), CONCAT(x.HOSPCODE,'-',x.PID),NULL)) A4T2

        FROM (SELECT n.HOSPCODE,n.PID,p.CID,n.SEQ,n.DATE_SERV,p.check_hosp,p.check_vhid,n.WEIGHT,n.HEIGHT,n.HEADCIRCUM,FOOD,BOTTLE
        ,p.BIRTH,p.SEX,p.NATION, IF(DATE_FORMAT(n.DATE_SERV,'%m') IN(10,11,12,1),2,
        IF(DATE_FORMAT(n.DATE_SERV,'%m') IN(5,6,7),1,0
        )) as term
        ,nutri_cal(TIMESTAMPDIFF(month,birth,date_serv),sex,1,height,weight) as nutri1
        ,nutri_cal(TIMESTAMPDIFF(month,birth,date_serv),sex,2,height,weight) as nutri2
        ,nutri_cal(TIMESTAMPDIFF(month,birth,date_serv),sex,3,height,weight) as nutri3
        ,t.tamboncodefull,t.tambonname
        FROM
        t_person_db p
        INNER JOIN chospital h ON h.hoscode=p.hospcode
        INNER JOIN student_r11 s on s.pid=p.pid and s.hospcode=p.hospcode
        LEFT JOIN  (select * from tmp_nutrition where DATE_SERV between :start_d and :end_d
        and WEIGHT between 0.1 and 300 and HEIGHT between 40 and 250 )  n
        ON n.HOSPCODE=p.HOSPCODE AND n.PID=p.PID
        LEFT JOIN school_r11 sc on sc.schoolcode=s.schoolcode and sc.hospcode=s.hospcode
        LEFT JOIN ctambon t on t.tamboncodefull=left(p.check_vhid,6)
        WHERE p.NATION in(99) 
        AND h.provcode=:changwatcode
        AND t.ampurcode=:ampurcode
        AND s.class=:class
        AND s.educationyear=:educationyear) x
        GROUP BY areacode
        WITH ROLLUP";

        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':start_d', $model->start_d);
        $cmd->bindValue(':end_d', $model->end_d);
        $cmd->bindValue(':changwatcode', $model->changwatcode);
        $cmd->bindValue(':ampurcode', $model->ampurcode);
        $cmd->bindValue(':class', $model->class);
        $cmd->bindValue(':educationyear', Cyear::GetYearName($model->byear));
        $data = $cmd->queryAll();

        return ['sql' => $sql, 'cmd' => $cmd, 'data' => $data];
    }

    public function getSQLTambonVillage($model)
    {
        $conn = Yii::$app->db;

        $sql = "SELECT x.villagecodefull as areacode,concat(' ',x.villagecode,' ',x.villagename) as areaname,x.check_hosp,x.check_vhid,DATE_FORMAT(now(),'%Y%m%d%H%i') as d_com

        ,COUNT(DISTINCT x.cid) as B0

        ,COUNT(DISTINCT if(x.term=1, CONCAT(x.HOSPCODE,'-',x.PID),NULL)) B1T1
        ,COUNT(DISTINCT if(x.term=2, CONCAT(x.HOSPCODE,'-',x.PID),NULL)) B1T2

        ,COUNT(DISTINCT if(x.term=1 AND nutri2 in(3,4,5) AND nutri3 in(3), CONCAT(x.HOSPCODE,'-',x.PID),NULL)) A1T1
        ,COUNT(DISTINCT if(x.term=2 AND nutri2 in(3,4,5) AND nutri3 in(3), CONCAT(x.HOSPCODE,'-',x.PID),NULL)) A1T2

        ,COUNT(DISTINCT if(x.term=1 AND nutri3 in(1), CONCAT(x.HOSPCODE,'-',x.PID),NULL)) A2T1
        ,COUNT(DISTINCT if(x.term=2 AND nutri3 in(1), CONCAT(x.HOSPCODE,'-',x.PID),NULL)) A2T2

        ,COUNT(DISTINCT if(x.term=1 AND nutri3 IN(5,6,7), CONCAT(x.HOSPCODE,'-',x.PID),NULL)) A3T1
        ,COUNT(DISTINCT if(x.term=2 AND nutri3 IN(5,6,7), CONCAT(x.HOSPCODE,'-',x.PID),NULL)) A3T2

        ,COUNT(DISTINCT if(x.term=1 AND nutri2 in(1), CONCAT(x.HOSPCODE,'-',x.PID),NULL)) A4T1
        ,COUNT(DISTINCT if(x.term=2 AND nutri2 in(1), CONCAT(x.HOSPCODE,'-',x.PID),NULL)) A4T2

        FROM (SELECT n.HOSPCODE,n.PID,p.CID,n.SEQ,n.DATE_SERV,p.check_hosp,p.check_vhid,n.WEIGHT,n.HEIGHT,n.HEADCIRCUM,FOOD,BOTTLE
        ,p.BIRTH,p.SEX,p.NATION, IF(DATE_FORMAT(n.DATE_SERV,'%m') IN(10,11,12,1),2,
        IF(DATE_FORMAT(n.DATE_SERV,'%m') IN(5,6,7),1,0
        )) as term
        ,nutri_cal(TIMESTAMPDIFF(month,birth,date_serv),sex,1,height,weight) as nutri1
        ,nutri_cal(TIMESTAMPDIFF(month,birth,date_serv),sex,2,height,weight) as nutri2
        ,nutri_cal(TIMESTAMPDIFF(month,birth,date_serv),sex,3,height,weight) as nutri3
        ,vl.villagecodefull,vl.villagecode,vl.villagename
        FROM
        t_person_db p
        INNER JOIN chospital h ON h.hoscode=p.hospcode
        INNER JOIN student_r11 s on s.pid=p.pid and s.hospcode=p.hospcode
        LEFT JOIN  (select * from tmp_nutrition where DATE_SERV between :start_d and :end_d
        and WEIGHT between 0.1 and 300 and HEIGHT between 40 and 250 )  n
        ON n.HOSPCODE=p.HOSPCODE AND n.PID=p.PID
        LEFT JOIN school_r11 sc on sc.schoolcode=s.schoolcode and sc.hospcode=s.hospcode
        LEFT JOIN cvillage vl on vl.villagecodefull=left(p.check_vhid,8)
        WHERE p.NATION in(99) 
        AND h.provcode=:changwatcode
        AND vl.tamboncode=:tamboncode
        AND s.class=:class
        AND s.educationyear=:educationyear) x
        GROUP BY areacode
        WITH ROLLUP";

        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':start_d', $model->start_d);
        $cmd->bindValue(':end_d', $model->end_d);
        $cmd->bindValue(':changwatcode', $model->changwatcode);
        $cmd->bindValue(':tamboncode', $model->tamboncode);
        $cmd->bindValue(':class', $model->class);
        $cmd->bindValue(':educationyear', Cyear::GetYearName($model->byear));
        $data = $cmd->queryAll();

        return ['sql' => $sql, 'cmd' => $cmd, 'data' => $data];
    }

    public function getSQLMastercup($model)
    {
        $conn = Yii::$app->db;

        $sql = "SELECT x.hoscode as areacode,x.hosname as areaname,x.check_hosp,x.check_vhid,DATE_FORMAT(now(),'%Y%m%d%H%i') as d_com

        ,COUNT(DISTINCT x.cid) as B0

        ,COUNT(DISTINCT if(x.term=1, CONCAT(x.HOSPCODE,'-',x.PID),NULL)) B1T1
        ,COUNT(DISTINCT if(x.term=2, CONCAT(x.HOSPCODE,'-',x.PID),NULL)) B1T2

        ,COUNT(DISTINCT if(x.term=1 AND nutri2 in(3,4,5) AND nutri3 in(3), CONCAT(x.HOSPCODE,'-',x.PID),NULL)) A1T1
        ,COUNT(DISTINCT if(x.term=2 AND nutri2 in(3,4,5) AND nutri3 in(3), CONCAT(x.HOSPCODE,'-',x.PID),NULL)) A1T2

        ,COUNT(DISTINCT if(x.term=1 AND nutri3 in(1), CONCAT(x.HOSPCODE,'-',x.PID),NULL)) A2T1
        ,COUNT(DISTINCT if(x.term=2 AND nutri3 in(1), CONCAT(x.HOSPCODE,'-',x.PID),NULL)) A2T2

        ,COUNT(DISTINCT if(x.term=1 AND nutri3 IN(5,6,7), CONCAT(x.HOSPCODE,'-',x.PID),NULL)) A3T1
        ,COUNT(DISTINCT if(x.term=2 AND nutri3 IN(5,6,7), CONCAT(x.HOSPCODE,'-',x.PID),NULL)) A3T2

        ,COUNT(DISTINCT if(x.term=1 AND nutri2 in(1), CONCAT(x.HOSPCODE,'-',x.PID),NULL)) A4T1
        ,COUNT(DISTINCT if(x.term=2 AND nutri2 in(1), CONCAT(x.HOSPCODE,'-',x.PID),NULL)) A4T2

        FROM (SELECT n.HOSPCODE,n.PID,p.CID,n.SEQ,n.DATE_SERV,p.check_hosp,p.check_vhid,n.WEIGHT,n.HEIGHT,n.HEADCIRCUM,FOOD,BOTTLE
        ,p.BIRTH,p.SEX,p.NATION, IF(DATE_FORMAT(n.DATE_SERV,'%m') IN(10,11,12,1),2,
        IF(DATE_FORMAT(n.DATE_SERV,'%m') IN(5,6,7),1,0
        )) as term
        ,nutri_cal(TIMESTAMPDIFF(month,birth,date_serv),sex,1,height,weight) as nutri1
        ,nutri_cal(TIMESTAMPDIFF(month,birth,date_serv),sex,2,height,weight) as nutri2
        ,nutri_cal(TIMESTAMPDIFF(month,birth,date_serv),sex,3,height,weight) as nutri3
        ,h.hoscode,h.hosname
        FROM
        t_person_db p
        INNER JOIN chospital h ON h.hoscode=p.hospcode
        INNER JOIN student_r11 s on s.pid=p.pid and s.hospcode=p.hospcode
        LEFT JOIN  (select * from tmp_nutrition where DATE_SERV between :start_d and :end_d
        and WEIGHT between 0.1 and 300 and HEIGHT between 40 and 250 )  n
        ON n.HOSPCODE=p.HOSPCODE AND n.PID=p.PID
        LEFT JOIN school_r11 sc on sc.schoolcode=s.schoolcode and sc.hospcode=s.hospcode
        INNER JOIN cmastercup c ON h.hoscode = c.hsub
        WHERE p.NATION in(99) 
        AND h.provcode=:changwatcode
        AND c.hmain=:mastercup
        AND s.class=:class
        AND s.educationyear=:educationyear) x
        GROUP BY areacode
        WITH ROLLUP";

        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':start_d', $model->start_d);
        $cmd->bindValue(':end_d', $model->end_d);
        $cmd->bindValue(':changwatcode', $model->changwatcode);
        $cmd->bindValue(':mastercup', $model->mastercup);
        $cmd->bindValue(':class', $model->class);
        $cmd->bindValue(':educationyear', Cyear::GetYearName($model->byear));
        $data = $cmd->queryAll();

        return ['sql' => $sql, 'cmd' => $cmd, 'data' => $data];
    }

    public function getSQLAmpurSchool($model)
    {
        $conn = Yii::$app->db;

        $sql = "SELECT x.schoolcode as areacode,x.schoolname as areaname,x.check_hosp,x.check_vhid,DATE_FORMAT(now(),'%Y%m%d%H%i') as d_com

        ,COUNT(DISTINCT x.cid) as B0

        ,COUNT(DISTINCT if(x.term=1, CONCAT(x.HOSPCODE,'-',x.PID),NULL)) B1T1
        ,COUNT(DISTINCT if(x.term=2, CONCAT(x.HOSPCODE,'-',x.PID),NULL)) B1T2

        ,COUNT(DISTINCT if(x.term=1 AND nutri2 in(3,4,5) AND nutri3 in(3), CONCAT(x.HOSPCODE,'-',x.PID),NULL)) A1T1
        ,COUNT(DISTINCT if(x.term=2 AND nutri2 in(3,4,5) AND nutri3 in(3), CONCAT(x.HOSPCODE,'-',x.PID),NULL)) A1T2

        ,COUNT(DISTINCT if(x.term=1 AND nutri3 in(1), CONCAT(x.HOSPCODE,'-',x.PID),NULL)) A2T1
        ,COUNT(DISTINCT if(x.term=2 AND nutri3 in(1), CONCAT(x.HOSPCODE,'-',x.PID),NULL)) A2T2

        ,COUNT(DISTINCT if(x.term=1 AND nutri3 IN(5,6,7), CONCAT(x.HOSPCODE,'-',x.PID),NULL)) A3T1
        ,COUNT(DISTINCT if(x.term=2 AND nutri3 IN(5,6,7), CONCAT(x.HOSPCODE,'-',x.PID),NULL)) A3T2

        ,COUNT(DISTINCT if(x.term=1 AND nutri2 in(1), CONCAT(x.HOSPCODE,'-',x.PID),NULL)) A4T1
        ,COUNT(DISTINCT if(x.term=2 AND nutri2 in(1), CONCAT(x.HOSPCODE,'-',x.PID),NULL)) A4T2

        FROM (SELECT n.HOSPCODE,n.PID,p.CID,n.SEQ,n.DATE_SERV,p.check_hosp,p.check_vhid,n.WEIGHT,n.HEIGHT,n.HEADCIRCUM,FOOD,BOTTLE
        ,p.BIRTH,p.SEX,p.NATION, IF(DATE_FORMAT(n.DATE_SERV,'%m') IN(10,11,12,1),2,
        IF(DATE_FORMAT(n.DATE_SERV,'%m') IN(5,6,7),1,0
        )) as term
        ,nutri_cal(TIMESTAMPDIFF(month,birth,date_serv),sex,1,height,weight) as nutri1
        ,nutri_cal(TIMESTAMPDIFF(month,birth,date_serv),sex,2,height,weight) as nutri2
        ,nutri_cal(TIMESTAMPDIFF(month,birth,date_serv),sex,3,height,weight) as nutri3
        ,sc.schoolcode,sc.schoolname
        FROM
        t_person_db p
        INNER JOIN chospital h ON h.hoscode=p.hospcode
        INNER JOIN student_r11 s on s.pid=p.pid and s.hospcode=p.hospcode
        LEFT JOIN  (select * from tmp_nutrition where DATE_SERV between  :start_d and :end_d
        and WEIGHT between 0.1 and 300 and HEIGHT between 40 and 250 )  n
        ON n.HOSPCODE=p.HOSPCODE AND n.PID=p.PID
        LEFT JOIN school_r11 sc on sc.schoolcode=s.schoolcode and sc.hospcode=s.hospcode
        LEFT JOIN campur ap on ap.ampurcodefull=left(sc.vid,4)
        WHERE p.NATION in(99) 
        AND h.provcode=:changwatcode
        AND ap.ampurcodefull=:ampurcode
        AND s.class=:class
        AND s.educationyear=:educationyear) x
        GROUP BY areacode
        WITH ROLLUP";

        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':start_d', $model->start_d);
        $cmd->bindValue(':end_d', $model->end_d);
        $cmd->bindValue(':changwatcode', $model->changwatcode);
        $cmd->bindValue(':ampurcode', $model->ampurcode);
        $cmd->bindValue(':class', $model->class);
        $cmd->bindValue(':educationyear', Cyear::GetYearName($model->byear));
        $data = $cmd->queryAll();

        return ['sql' => $sql, 'cmd' => $cmd, 'data' => $data];
    }

}
