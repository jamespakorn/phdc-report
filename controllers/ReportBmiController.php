<?php
namespace app\controllers;

use app\models\Ctambon;
use app\models\ReportForm;
use Yii;
use yii\data\ArrayDataProvider;
use yii\helpers\Json;

class ReportBmiController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $model = new ReportForm;
        $model->byear = date('Y')-5;
        $model->start_d = ((int) $model->byear - 1) . '-10-01';
        $model->end_d = $model->byear . '-09-30';
        $model->changwatcode = Yii::$app->params['provinceCode'];
        $model->ampurcode = null;
        $reportType = 1;
        $ampurStyle = null;
        $tambonCode = null;
        $model->ageMin = 35;
        $model->ageMax = 35;

        //POST
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            $post = (object) Yii::$app->request->post('ReportForm');
            $model->byear = $post->byear;
            $model->changwatcode = $post->changwatcode;
            $model->ampurcode = $post->ampurcode;
            $model->start_d = ((int) $model->byear - 1) . '-10-01';
            $model->end_d = $model->byear . '-09-30';
            $model->mastercup = $post->mastercup;
            // $model->ageMin = $post->ageMin;
            // $model->ageMax = $post->ageMax;

            //check ampurstyle
            if (isset($post->ampurstyle)) {
                $ampurStyle = $post->ampurstyle;
                $model->ampurstyle = $post->ampurstyle;

                //check tamboncode
                if (isset($post->tamboncode)) {
                    $tambonCode = $post->tamboncode;
                    $model->tamboncode = $post->tamboncode;
                }

            }

            if ($post->mastercup != '') {
                $reportType = 5;
            } else if ($post->ampurcode !== '') {
                if ($ampurStyle == null | $ampurStyle == '01') {
                    $reportType = 2;
                } else if ($ampurStyle == '02') {
                    //Tambon
                    if ($tambonCode == null) {
                        $reportType = 3;
                    } else {
                        $reportType = 4;
                    }
                }
            }

        }

        if ($reportType == 1) {
            $result = (object) $this->getSQLChangwat($model);
        } else if ($reportType == 2) {
            $result = (object) $this->getSQLAmpurHospital($model);
        } else if ($reportType == 3) {
            $result = (object) $this->getSQLAmpurTambon($model);
        } else if ($reportType == 4) {
            $result = (object) $this->getSQLTambonVillage($model);
        } else if ($reportType == 5) {
            $result = (object) $this->getSQLMastercup($model);
        }

        $dataProvider = new ArrayDataProvider([
            'allModels' => $result->data,
            'pagination' => false,
        ]);

        return $this->render('index', [
            'model' => $model,
            'sql' => $result->sql,
            'sqlParams' => implode(', ', $result->cmd->params),
            'reportType' => $reportType,
            'dataProvider' => $dataProvider]);
    }

    public function getReportTypeName($type)
    {
        if ($type === 1) {
            return 'อำเภอ';
        } else if ($type === 2) {
            return 'หน่วยบริการ';
        } else if ($type === 3) {
            return 'ตำบล';
        } else if ($type === 4) {
            return 'หมู่บ้าน';
        } else if ($type === 5) {
            return 'เครือข่ายบริการสุขภาพ';
        }
    }

    public function actionDepdropampurstyle()
    {
        $out = [];
        if (Yii::$app->request->post('depdrop_parents')) {
            $parents = Yii::$app->request->post('depdrop_parents');
            if ($parents != null) {
                if ($parents[0] !== '') {
                    $out = [
                        ['id' => '01', 'name' => 'หน่วยบริการ'],
                        ['id' => '02', 'name' => 'ตำบล/แขวง'],
                    ];
                    return Json::encode(['output' => $out, 'selected' => '']);
                } else {
                    return Json::encode(['output' => $out, 'selected' => '']);
                }

            }
        }

        return Json::encode(['output' => $out, 'selected' => '']);
    }

    public function Ampurstyledata($id)
    {

        if ($id == '01') {
            return ['01' => 'หน่วยบริการ'];
        } else if ($id == '02') {
            return ['02' => 'ตำบล/แขวง'];
        } else {
            return [];
        }
    }

    public function actionDepdroptambon()
    {
        $out = [];
        if (Yii::$app->request->post('depdrop_parents')) {
            $ids = Yii::$app->request->post('depdrop_parents');
            $ampurcode = empty($ids[0]) ? null : $ids[0];
            $ampurstyle = empty($ids[1]) ? null : $ids[1];
            if ($ampurcode != null && $ampurstyle == '02') {
                $out = Ctambon::find()
                    ->where(['ampurcode' => $ampurcode])
                    ->andWhere(['flag_status' => 0])
                    ->select(['tamboncodefull AS id', 'tambonname AS name'])->asArray()->all();
                return Json::encode(['output' => $out, 'selected' => '']);
            } else {
                return Json::encode(['output' => $out, 'selected' => '']);
            }

        }
        return Json::encode(['output' => '', 'selected' => '']);
    }

    public function getSQLChangwat($model)
    {
        $conn = Yii::$app->db;

        $sql = "SELECT ap.ampurcodefull as areacode,ap.ampurname as areaname,p.check_hosp,p.check_vhid
        ,DATE_FORMAT(now(),'%Y%m%d%H%i') as d_com
        ,COUNT(DISTINCT p.cid) as target
        ,COUNT(DISTINCT IF(ROUND(weight/((height/100)*(height/100)),2) BETWEEN 18.5 AND 22.9 ,p.cid,null)) as result
        ,COUNT(DISTINCT IF(ROUND(weight/((height/100)*(height/100)),2) BETWEEN 18.5 AND 22.9 ,p.cid,null))
        /COUNT(DISTINCT p.cid)*100 as  percent

        FROM tmp_ncdscreen n
        INNER JOIN t_person_db p ON n.HOSPCODE=p.HOSPCODE AND n.PID=p.PID
        INNER JOIN chospital h ON n.HOSPCODE=h.hoscode
        LEFT JOIN campur ap on ap.ampurcodefull=left(p.check_vhid,4)
        WHERE n.DATE_SERV BETWEEN :start_d AND :end_d
        AND n.weight > 0 AND n.height >0
        AND h.provcode=:changwatcode
        AND TIMESTAMPDIFF(YEAR,p.BIRTH,n.DATE_SERV) BETWEEN :min AND :max
        GROUP BY areacode
        WITH ROLLUP";

        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':start_d', $model->start_d);
        $cmd->bindValue(':end_d', $model->end_d);
        $cmd->bindValue(':changwatcode', $model->changwatcode);
        $cmd->bindValue(':min', $model->ageMin);
        $cmd->bindValue(':max', $model->ageMax);
        $data = $cmd->queryAll();

        return ['sql' => $sql, 'cmd' => $cmd, 'data' => $data];
    }

    public function getSQLAmpurHospital($model)
    {
        $conn = Yii::$app->db;

        $sql = "SELECT h.hoscode as areacode,h.hosname as areaname,p.check_hosp,p.check_vhid
        ,DATE_FORMAT(now(),'%Y%m%d%H%i') as d_com
        ,COUNT(DISTINCT p.cid) as target
        ,COUNT(DISTINCT IF(ROUND(weight/((height/100)*(height/100)),2) BETWEEN 18.5 AND 22.9 ,p.cid,null)) as result
        ,COUNT(DISTINCT IF(ROUND(weight/((height/100)*(height/100)),2) BETWEEN 18.5 AND 22.9 ,p.cid,null))
        /COUNT(DISTINCT p.cid)*100 as  percent

        FROM tmp_ncdscreen n
        INNER JOIN t_person_db p ON n.HOSPCODE=p.HOSPCODE AND n.PID=p.PID
        INNER JOIN chospital h ON n.HOSPCODE=h.hoscode
        LEFT JOIN campur ap on ap.ampurcodefull=left(p.check_vhid,4)
        WHERE n.DATE_SERV BETWEEN :start_d AND :end_d
        AND n.weight > 0 AND n.height >0
        AND h.provcode=:changwatcode
        AND TIMESTAMPDIFF(YEAR,p.BIRTH,n.DATE_SERV) BETWEEN :min AND :max
        AND h.hdc_regist=1 AND CONCAT(h.provcode,h.distcode)=:ampurcode
        GROUP BY areacode
        WITH ROLLUP";

        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':start_d', $model->start_d);
        $cmd->bindValue(':end_d', $model->end_d);
        $cmd->bindValue(':changwatcode', $model->changwatcode);
        $cmd->bindValue(':min', $model->ageMin);
        $cmd->bindValue(':max', $model->ageMax);
        $cmd->bindValue(':ampurcode', $model->ampurcode);
        $data = $cmd->queryAll();

        return ['sql' => $sql, 'cmd' => $cmd, 'data' => $data];
    }

    public function getSQLAmpurTambon($model)
    {
        $conn = Yii::$app->db;

        $sql = "SELECT t.tamboncodefull as areacode,t.tambonname as areaname,p.check_hosp,p.check_vhid
        ,DATE_FORMAT(now(),'%Y%m%d%H%i') as d_com
        ,COUNT(DISTINCT p.cid) as target
        ,COUNT(DISTINCT IF(ROUND(weight/((height/100)*(height/100)),2) BETWEEN 18.5 AND 22.9 ,p.cid,null)) as result
        ,COUNT(DISTINCT IF(ROUND(weight/((height/100)*(height/100)),2) BETWEEN 18.5 AND 22.9 ,p.cid,null))
        /COUNT(DISTINCT p.cid)*100 as  percent

        FROM tmp_ncdscreen n
        INNER JOIN t_person_db p ON n.HOSPCODE=p.HOSPCODE AND n.PID=p.PID
        INNER JOIN chospital h ON n.HOSPCODE=h.hoscode
        LEFT JOIN ctambon t on t.tamboncodefull=left(p.check_vhid,6)
        WHERE n.DATE_SERV BETWEEN :start_d AND :end_d
        AND n.weight > 0 AND n.height >0
        AND h.provcode=:changwatcode
        AND TIMESTAMPDIFF(YEAR,p.BIRTH,n.DATE_SERV) BETWEEN :min AND :max
        AND t.ampurcode=:ampurcode
        GROUP BY areacode
        WITH ROLLUP";

        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':start_d', $model->start_d);
        $cmd->bindValue(':end_d', $model->end_d);
        $cmd->bindValue(':changwatcode', $model->changwatcode);
        $cmd->bindValue(':min', (int) $model->ageMin);
        $cmd->bindValue(':max', (int) $model->ageMax);
        $cmd->bindValue(':ampurcode', $model->ampurcode);
        $data = $cmd->queryAll();

        return ['sql' => $sql, 'cmd' => $cmd, 'data' => $data];
    }

    public function getSQLTambonVillage($model)
    {
        $conn = Yii::$app->db;

        $sql = "SELECT v.villagecodefull as areacode,concat(' ',v.villagecode,' ',v.villagename) as areaname,p.check_hosp,p.check_vhid
        ,DATE_FORMAT(now(),'%Y%m%d%H%i') as d_com
        ,COUNT(DISTINCT p.cid) as target
        ,COUNT(DISTINCT IF(ROUND(weight/((height/100)*(height/100)),2) BETWEEN 18.5 AND 22.9 ,p.cid,null)) as result
        ,COUNT(DISTINCT IF(ROUND(weight/((height/100)*(height/100)),2) BETWEEN 18.5 AND 22.9 ,p.cid,null))
        /COUNT(DISTINCT p.cid)*100 as  percent

        FROM tmp_ncdscreen n
        INNER JOIN t_person_db p ON n.HOSPCODE=p.HOSPCODE AND n.PID=p.PID
        INNER JOIN chospital h ON n.HOSPCODE=h.hoscode
        LEFT JOIN cvillage v on v.villagecodefull=left(p.check_vhid,8)
        WHERE n.DATE_SERV BETWEEN :start_d AND :end_d
        AND n.weight > 0 AND n.height >0
        AND h.provcode=:changwatcode
        AND TIMESTAMPDIFF(YEAR,p.BIRTH,n.DATE_SERV) BETWEEN :min AND :max
        AND v.tamboncode=:tamboncode
        GROUP BY areacode
        WITH ROLLUP";

        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':start_d', $model->start_d);
        $cmd->bindValue(':end_d', $model->end_d);
        $cmd->bindValue(':changwatcode', $model->changwatcode);
        $cmd->bindValue(':min', $model->ageMin);
        $cmd->bindValue(':max', $model->ageMax);
        $cmd->bindValue(':tamboncode', $model->tamboncode);
        $data = $cmd->queryAll();

        return ['sql' => $sql, 'cmd' => $cmd, 'data' => $data];
    }

    public function getSQLMastercup($model)
    {
        $conn = Yii::$app->db;

        $sql = "SELECT h.hoscode as areacode,h.hosname as areaname,p.check_hosp,p.check_vhid
        ,DATE_FORMAT(now(),'%Y%m%d%H%i') as d_com
        ,COUNT(DISTINCT p.cid) as target
        ,COUNT(DISTINCT IF(ROUND(weight/((height/100)*(height/100)),2) BETWEEN 18.5 AND 22.9 ,p.cid,null)) as result
        ,COUNT(DISTINCT IF(ROUND(weight/((height/100)*(height/100)),2) BETWEEN 18.5 AND 22.9 ,p.cid,null))
        /COUNT(DISTINCT p.cid)*100 as  percent

        FROM tmp_ncdscreen n
        INNER JOIN t_person_db p ON n.HOSPCODE=p.HOSPCODE AND n.PID=p.PID
        INNER JOIN chospital h ON n.HOSPCODE=h.hoscode
        INNER JOIN cmastercup c ON h.hoscode = c.hsub
        WHERE n.DATE_SERV BETWEEN :start_d AND :end_d
        AND n.weight > 0 AND n.height >0
        AND h.provcode=:changwatcode
        AND TIMESTAMPDIFF(YEAR,p.BIRTH,n.DATE_SERV) BETWEEN :min AND :max
        AND c.hmain=:mastercup
        GROUP BY areacode
        WITH ROLLUP";

        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':start_d', $model->start_d);
        $cmd->bindValue(':end_d', $model->end_d);
        $cmd->bindValue(':changwatcode', $model->changwatcode);
        $cmd->bindValue(':min', $model->ageMin);
        $cmd->bindValue(':max', $model->ageMax);
        $cmd->bindValue(':mastercup', $model->mastercup);
        $data = $cmd->queryAll();

        return ['sql' => $sql, 'cmd' => $cmd, 'data' => $data];
    }

}
