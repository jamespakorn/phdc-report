<?php
namespace app\controllers;

use Yii;

class CheckSchoolController extends \yii\web\Controller
{
    public function actionIndex()
    {
        // $model = new ReportForm;
        // $model->eyear=2017;

        //POST
        // if ($model->load(Yii::$app->request->post()) && $model->validate()) {

        //     $post = (object) Yii::$app->request->post('ReportForm');
        //     $model->eyear=$post->eyear;

        // }

        $conn = Yii::$app->db;

        $sql = "SELECT h.hoscode,h.hosname,GROUP_CONCAT(' | ',s.schoolname SEPARATOR ' ') as schoolname
            FROM school_r11 s
            LEFT JOIN chospital h ON h.hoscode=s.hospcode
            GROUP BY h.hoscode";

        $cmd = $conn->createCommand($sql);
        // $cmd->bindValue(':educationyear', (int)$model->eyear+543);
        $data = $cmd->queryAll();

        return $this->render('index', [
            'data' => $data]);
    }

}
