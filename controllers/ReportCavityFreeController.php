<?php
namespace app\controllers;

use app\models\Ctambon;
use app\models\ReportForm;
use Yii;
use yii\data\ArrayDataProvider;
use yii\helpers\Json;
use app\models\Cyear;

class ReportCavityFreeController extends \yii\web\Controller
{
    public $class = [
        '11' => 'ป.1', '12' => 'ป.2', '13' => 'ป.3',
        '21' => 'ป.4', '22' => 'ป.5', '23' => 'ป.6',
        '31' => 'ม.1', '32' => 'ม.2', '33' => 'ม.3',
        '41' => 'ม.4', '42' => 'ม.5', '43' => 'ม.6',

    ];

    public $mSQL1 = "
    DATE_FORMAT(now(),'%Y%m%d%H%i') as d_com

    ,COUNT(DISTINCT IF(v.providertype IN('02','06') 
    AND d.PTEETH BETWEEN :pteeth1 AND :pteeth2 
    AND (d.PFILLING+d.PEXTRACT+d.PCARIES) <=:ppp
    AND (d.PCARIES+d.PFILLING) <=PTEETH
    AND d.DATE_SERV BETWEEN :start_d AND :end_d,d.cid,null)) as  target
    
    ,COUNT(DISTINCT IF(v.providertype IN('02','06') 
    AND d.PTEETH BETWEEN :pteeth1 AND :pteeth2 
    AND (d.PFILLING+d.PEXTRACT+d.PCARIES) <=:ppp
    AND (d.PCARIES+d.PFILLING) <=PTEETH
    AND PFILLING>=0 AND PEXTRACT=0 AND PCARIES=0
    AND d.DATE_SERV BETWEEN :start_d AND :end_d,d.cid,null)) as  result
    
    ,COUNT(DISTINCT IF(v.providertype IN('02','06') 
    AND d.PTEETH BETWEEN :pteeth1 AND :pteeth2 
    AND (d.PFILLING+d.PEXTRACT+d.PCARIES) <=:ppp
    AND (d.PCARIES+d.PFILLING) <=PTEETH
    AND PFILLING>=0 AND PEXTRACT=0 AND PCARIES=0
    AND d.DATE_SERV BETWEEN :start_d AND :end_d,d.cid,null))
    /COUNT(DISTINCT IF(v.providertype IN('02','06') 
    AND d.PTEETH BETWEEN :pteeth1 AND :pteeth2 
    AND (d.PFILLING+d.PEXTRACT+d.PCARIES) <=:ppp
    AND (d.PCARIES+d.PFILLING) <=PTEETH
    AND d.DATE_SERV BETWEEN :start_d AND :end_d,d.cid,null))*100 as percent";

    public $mSQL2 = "
    FROM
    t_person_db p
    INNER JOIN chospital h ON h.hoscode=p.hospcode
    LEFT JOIN tmp_dental d ON d.hospcode=p.hospcode AND d.pid=p.pid
    INNER JOIN student_r11 s on s.hospcode=p.hospcode AND s.pid=p.pid
    LEFT JOIN provider v ON d.hospcode=v.hospcode AND d.provider=v.provider";

    public function actionIndex()
    {
        $model = new ReportForm;
        $model->byear = date('Y')-5;
        $model->start_d = ((int) $model->byear) . '-05-01';
        $model->end_d = ((int) $model->byear+1) . '-04-30';
        $model->changwatcode = Yii::$app->params['provinceCode'];
        $model->ampurcode = null;
        $reportType = 1;
        $ampurStyle = null;
        $tambonCode = null;
        $model->ageMin = 30;
        $model->ageMax = 44;
        $model->class = 11;
        //ป.1        
        $model->pteeth1 = 1;
        $model->pteeth2 = 12;
        $model->ppp = 12;

        //POST
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            $post = (object) Yii::$app->request->post('ReportForm');
            $model->byear = $post->byear;
            $model->changwatcode = $post->changwatcode;
            $model->ampurcode = $post->ampurcode;
            $model->start_d = ((int) $model->byear) . '-05-01';
            $model->end_d = ((int) $model->byear+1) . '-04-30';
            $model->mastercup = $post->mastercup;
            $model->class = $post->class;
            $model->cupstyle=$post->cupstyle;

            //ป.1
            if ($model->class == '11') {
                $model->pteeth1 = 1;
                $model->pteeth2 = 12;
                $model->ppp = 12;
            } else if ($model->class == '12' || $model->class == '13') {
                $model->pteeth1 = 1;
                $model->pteeth2 = 24;
                $model->ppp = 24;

            } else if ($model->class == '21' || $model->class == '22' || $model->class == '23') {
                $model->pteeth1 = 1;
                $model->pteeth2 = 28;
                $model->ppp = 28;
            }

            //check ampurstyle
            if (isset($post->ampurstyle)) {
                $ampurStyle = $post->ampurstyle;
                $model->ampurstyle = $post->ampurstyle;

                //check tamboncode
                if (isset($post->tamboncode)) {
                    $tambonCode = $post->tamboncode;
                    $model->tamboncode = $post->tamboncode;
                }

            }

            if ($post->mastercup != '') {
                $post->cupstyle == '02' ? $reportType = 7 : $reportType = 5;
            } else if ($post->ampurcode !== '') {
                if ($ampurStyle == null | $ampurStyle == '01') {
                    $reportType = 2;
                } else if ($ampurStyle == '02') {
                    //Tambon
                    if ($tambonCode == null) {
                        $reportType = 3;
                    } else {
                        $reportType = 4;
                    }
                } else if ($ampurStyle == '03') {
                    $reportType = 6;
                }
            }

        }

        if ($reportType == 1) {
            $result = (object) $this->getSQLChangwat($model);
        } else if ($reportType == 2) {
            $result = (object) $this->getSQLAmpurHospital($model);
        } else if ($reportType == 3) {
            $result = (object) $this->getSQLAmpurTambon($model);
        } else if ($reportType == 4) {
            $result = (object) $this->getSQLTambonVillage($model);
        } else if ($reportType == 5) {
            $result = (object) $this->getSQLMastercup($model);
        } else if ($reportType == 6) {
            $result = (object) $this->getSQLAmpurSchool($model);
        }else if ($reportType == 7) {
            $result = (object) $this->getSQLMastercupSchool($model);
        }


        $dataProvider = new ArrayDataProvider([
            'allModels' => $result->data,
            'pagination' => false,
        ]);

        return $this->render('index', [
            'model' => $model,
            'sql' => $result->sql,
            'sqlParams' => implode(', ', $result->cmd->params),
            'reportType' => $reportType,
            'dataProvider' => $dataProvider]);
    }

    public function getReportTypeName($type)
    {
        if ($type === 1) {
            return 'อำเภอ';
        } else if ($type === 2) {
            return 'หน่วยบริการ';
        } else if ($type === 3) {
            return 'ตำบล';
        } else if ($type === 4) {
            return 'หมู่บ้าน';
        } else if ($type === 5) {
            return 'เครือข่ายบริการสุขภาพ';
        } else if ($type === 6) {
            return 'โรงเรียน';
        } else if ($type === 7) {
            return 'โรงเรียน';
        }
    }


    public function actionDepdropampurstyle()
    {
        $out = [];
        if (Yii::$app->request->post('depdrop_parents')) {
            $parents = Yii::$app->request->post('depdrop_parents');
            if ($parents != null) {
                if ($parents[0] !== '') {
                    $out = [
                        ['id' => '01', 'name' => 'หน่วยบริการ'],
                        ['id' => '02', 'name' => 'ตำบล/แขวง'],
                        ['id' => '03', 'name' => 'โรงเรียน'],
                    ];
                    return Json::encode(['output' => $out, 'selected' => '']);
                } else {
                    return Json::encode(['output' => $out, 'selected' => '']);
                }

            }
        }

        return Json::encode(['output' => $out, 'selected' => '']);
    }

    public function Ampurstyledata($id)
    {

        if ($id == '01') {
            return ['01' => 'หน่วยบริการ'];
        } else if ($id == '02') {
            return ['02' => 'ตำบล/แขวง'];
        } else if ($id == '03') {
            return ['03' => 'โรงเรียน'];
        } else {
            return [];
        }
    }

    public function actionDepdroptambon()
    {
        $out = [];
        if (Yii::$app->request->post('depdrop_parents')) {
            $ids = Yii::$app->request->post('depdrop_parents');
            $ampurcode = empty($ids[0]) ? null : $ids[0];
            $ampurstyle = empty($ids[1]) ? null : $ids[1];
            if ($ampurcode != null && $ampurstyle == '02') {
                $out = Ctambon::find()
                    ->where(['ampurcode' => $ampurcode])
                    ->andWhere(['flag_status' => 0])
                    ->select(['tamboncodefull AS id', 'tambonname AS name'])->asArray()->all();
                return Json::encode(['output' => $out, 'selected' => '']);
            } else {
                return Json::encode(['output' => $out, 'selected' => '']);
            }

        }
        return Json::encode(['output' => '', 'selected' => '']);
    }

    public function getClass()
    {
        return $this->class;
    }

    public function getClassName($id)
    {
        if (($className = $this->class[$id]) !== null) {
            return $className;
        } else {
            return null;
        }
    }

    public function getSQLChangwat($model)
    {
        $conn = Yii::$app->db;

        $s[] = "SELECT ap.ampurcodefull as areacode,ap.ampurname as areaname,";
        array_push($s, $this->mSQL1);
        array_push($s, $this->mSQL2);
        array_push($s, "
        LEFT JOIN campur ap on ap.ampurcodefull=left(p.check_vhid,4)");
        array_push($s, "
        WHERE h.provcode=:changwatcode
        AND s.class=:class
        AND s.educationyear=:educationyear
        GROUP BY areacode
        WITH ROLLUP");
        $sql = implode(" ", $s);

        

        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':pteeth1', $model->pteeth1);
        $cmd->bindValue(':pteeth2', $model->pteeth2);
        $cmd->bindValue(':ppp', $model->ppp);
        $cmd->bindValue(':start_d', $model->start_d);
        $cmd->bindValue(':end_d', $model->end_d);
        $cmd->bindValue(':changwatcode', $model->changwatcode);
        $cmd->bindValue(':class', $model->class);
        $cmd->bindValue(':educationyear', Cyear::GetYearName($model->byear));
        $data = $cmd->queryAll();

        return ['sql' => $sql, 'cmd' => $cmd, 'data' => $data];
    }

    public function getSQLAmpurHospital($model)
    {
        $conn = Yii::$app->db;

        $s[] = "SELECT h.hoscode as areacode,h.hosname as areaname,";
        array_push($s, $this->mSQL1);
        array_push($s, $this->mSQL2);
        array_push($s, "
        LEFT JOIN campur ap on ap.ampurcodefull=left(p.check_vhid,4)");
        array_push($s, "
        WHERE h.provcode=:changwatcode
        AND h.hdc_regist=1 AND CONCAT(h.provcode,h.distcode)=:ampurcode
        AND s.class=:class
        AND s.educationyear=:educationyear
        GROUP BY areacode
        WITH ROLLUP");
        $sql = implode(" ", $s);

        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':pteeth1', $model->pteeth1);
        $cmd->bindValue(':pteeth2', $model->pteeth2);
        $cmd->bindValue(':ppp', $model->ppp);
        $cmd->bindValue(':start_d', $model->start_d);
        $cmd->bindValue(':end_d', $model->end_d);
        $cmd->bindValue(':changwatcode', $model->changwatcode);
        $cmd->bindValue(':ampurcode', $model->ampurcode);
        $cmd->bindValue(':class', $model->class);
        $cmd->bindValue(':educationyear', Cyear::GetYearName($model->byear));
        $data = $cmd->queryAll();

        return ['sql' => $sql, 'cmd' => $cmd, 'data' => $data];
    }

    public function getSQLAmpurTambon($model)
    {
        $conn = Yii::$app->db;

        $s[] = "SELECT t.tamboncodefull as areacode,t.tambonname as areaname,";
        array_push($s, $this->mSQL1);
        array_push($s, $this->mSQL2);
        array_push($s, "
        LEFT JOIN ctambon t on t.tamboncodefull=left(p.check_vhid,6)");
        array_push($s, "
        WHERE h.provcode=:changwatcode
        AND t.ampurcode=:ampurcode
        AND s.class=:class
        AND s.educationyear=:educationyear
        GROUP BY areacode
        WITH ROLLUP");
        $sql = implode(" ", $s);


        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':pteeth1', $model->pteeth1);
        $cmd->bindValue(':pteeth2', $model->pteeth2);
        $cmd->bindValue(':ppp', $model->ppp);
        $cmd->bindValue(':start_d', $model->start_d);
        $cmd->bindValue(':end_d', $model->end_d);
        $cmd->bindValue(':changwatcode', $model->changwatcode);
        $cmd->bindValue(':ampurcode', $model->ampurcode);
        $cmd->bindValue(':class', $model->class);
        $cmd->bindValue(':educationyear', Cyear::GetYearName($model->byear));
        $data = $cmd->queryAll();

        return ['sql' => $sql, 'cmd' => $cmd, 'data' => $data];
    }

    public function getSQLTambonVillage($model)
    {
        $conn = Yii::$app->db;

        $s[] = "SELECT vl.villagecodefull as areacode,concat(' ',vl.villagecode,' ',vl.villagename) as areaname,";
        array_push($s, $this->mSQL1);
        array_push($s, $this->mSQL2);
        array_push($s, "
        LEFT JOIN cvillage vl on vl.villagecodefull=left(p.check_vhid,8)");
        array_push($s, "
        WHERE h.provcode=:changwatcode
        AND vl.tamboncode=:tamboncode
        AND s.class=:class
        AND s.educationyear=:educationyear
        GROUP BY areacode
        WITH ROLLUP");
        $sql = implode(" ", $s);

        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':pteeth1', $model->pteeth1);
        $cmd->bindValue(':pteeth2', $model->pteeth2);
        $cmd->bindValue(':ppp', $model->ppp);
        $cmd->bindValue(':start_d', $model->start_d);
        $cmd->bindValue(':end_d', $model->end_d);
        $cmd->bindValue(':changwatcode', $model->changwatcode);
        $cmd->bindValue(':tamboncode', $model->tamboncode);
        $cmd->bindValue(':class', $model->class);
        $cmd->bindValue(':educationyear', Cyear::GetYearName($model->byear));
        $data = $cmd->queryAll();

        return ['sql' => $sql, 'cmd' => $cmd, 'data' => $data];
    }

    public function getSQLMastercup($model)
    {
        $conn = Yii::$app->db;

        $s[] = "SELECT h.hoscode as areacode,h.hosname as areaname,";
        array_push($s, $this->mSQL1);
        array_push($s, $this->mSQL2);
        array_push($s, "
        LEFT JOIN cmastercup c ON h.hoscode = c.hsub");
        array_push($s, "
        WHERE h.provcode=:changwatcode
        AND c.hmain=:mastercup
        AND s.class=:class
        AND s.educationyear=:educationyear
        GROUP BY areacode
        WITH ROLLUP");
        $sql = implode(" ", $s);

        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':pteeth1', $model->pteeth1);
        $cmd->bindValue(':pteeth2', $model->pteeth2);
        $cmd->bindValue(':ppp', $model->ppp);
        $cmd->bindValue(':start_d', $model->start_d);
        $cmd->bindValue(':end_d', $model->end_d);
        $cmd->bindValue(':changwatcode', $model->changwatcode);
        $cmd->bindValue(':mastercup', $model->mastercup);
        $cmd->bindValue(':class', $model->class);
        $cmd->bindValue(':educationyear', Cyear::GetYearName($model->byear));
        $data = $cmd->queryAll();

        return ['sql' => $sql, 'cmd' => $cmd, 'data' => $data];
    }

    public function getSQLAmpurSchool($model)
    {
        $conn = Yii::$app->db;

        $s[] = "SELECT concat(sc.hospcode,'-',sc.schoolcode) as areacode,sc.schoolname as areaname,";
        array_push($s, $this->mSQL1);
        array_push($s, $this->mSQL2);
        array_push($s, "
        LEFT JOIN school_r11 sc on sc.schoolcode=s.schoolcode and sc.hospcode=s.hospcode
        LEFT JOIN campur ap on ap.ampurcodefull=left(sc.vid,4)");
        array_push($s, "
        WHERE h.provcode=:changwatcode
        AND ap.ampurcodefull=:ampurcode
        AND s.class=:class
        AND s.educationyear=:educationyear
        GROUP BY areacode
        WITH ROLLUP");
        $sql = implode(" ", $s);

        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':pteeth1', $model->pteeth1);
        $cmd->bindValue(':pteeth2', $model->pteeth2);
        $cmd->bindValue(':ppp', $model->ppp);
        $cmd->bindValue(':start_d', $model->start_d);
        $cmd->bindValue(':end_d', $model->end_d);
        $cmd->bindValue(':changwatcode', $model->changwatcode);
        $cmd->bindValue(':ampurcode', $model->ampurcode);
        $cmd->bindValue(':class', $model->class);
        $cmd->bindValue(':educationyear', Cyear::GetYearName($model->byear));
        $data = $cmd->queryAll();

        return ['sql' => $sql, 'cmd' => $cmd, 'data' => $data];
    }

    public function getSQLMastercupSchool($model)
    {
        $conn = Yii::$app->db;

        $s[] = "SELECT concat(sc.hospcode,'-',sc.schoolcode) as areacode,sc.schoolname as areaname,";
        array_push($s, $this->mSQL1);
        array_push($s, $this->mSQL2);
        array_push($s, "
        LEFT JOIN cmastercup c ON h.hoscode = c.hsub
        LEFT JOIN school_r11 sc on  sc.schoolcode=s.schoolcode AND sc.hospcode=s.hospcode");
        array_push($s, "
        WHERE h.provcode=:changwatcode
        AND c.hmain=:mastercup
        AND s.class=:class
        AND s.educationyear=:educationyear
        GROUP BY areacode
        WITH ROLLUP");
        $sql = implode(" ", $s);

        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':pteeth1', $model->pteeth1);
        $cmd->bindValue(':pteeth2', $model->pteeth2);
        $cmd->bindValue(':ppp', $model->ppp);
        $cmd->bindValue(':start_d', $model->start_d);
        $cmd->bindValue(':end_d', $model->end_d);
        $cmd->bindValue(':changwatcode', $model->changwatcode);
        $cmd->bindValue(':mastercup', $model->mastercup);
        $cmd->bindValue(':class', $model->class);
        $cmd->bindValue(':educationyear', Cyear::GetYearName($model->byear));
        $data = $cmd->queryAll();

        return ['sql' => $sql, 'cmd' => $cmd, 'data' => $data];
    }

}
