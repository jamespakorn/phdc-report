<?php
namespace app\controllers;

use app\models\ReportForm;
use Yii;

class WsGoodHeight12yController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $model = new ReportForm;
        $model->byear = date('Y')-5;
        $model->start_d = ((int) $model->byear - 1) . '-10-01';
        $model->end_d = $model->byear . '-09-30';

        $conn = Yii::$app->db;

        //POST
        if (Yii::$app->request->post('ReportForm')) {

            $post = (object) Yii::$app->request->post('ReportForm');
            $model->byear = $post->byear;
            $model->start_d = ((int) $model->byear - 1) . '-10-01';
            $model->end_d = $model->byear . '-09-30';

        }

        $sql = "select hoscode as areacode,hosname as areaname,
        COUNT(DISTINCT cid) as target,
        COUNT(DISTINCT  if(nutri2 in(3,4,5) AND nutri3 in(3),cid,NULL)) result,
        COUNT(DISTINCT  if(nutri2 in(3,4,5) AND nutri3 in(3),cid,NULL))/COUNT(DISTINCT cid)*100 as percent
        from
       (select n.HOSPCODE,n.pid,n.cid ,n.DATE_SERV,TIMESTAMPDIFF(month,p.BIRTH,n.DATE_SERV) as age_m,
          nutri_cal(TIMESTAMPDIFF(month,p.BIRTH,n.DATE_SERV),p.SEX,1,height,weight) as nutri1,
          nutri_cal(TIMESTAMPDIFF(month,p.BIRTH,n.DATE_SERV),sex,2,height,weight) as nutri2,
          nutri_cal(TIMESTAMPDIFF(month,p.BIRTH,n.DATE_SERV),sex,3,height,weight) as nutri3,
        h.hoscode,h.hosname
       from t_person_cid p
       inner join tmp_nutrition n on n.cid=p.CID
       INNER JOIN chospital h ON p.check_hosp=h.hoscode
       where n.DATE_SERV BETWEEN :start_d AND :end_d
       and p.check_typearea in(1,3) AND p.NATION in(99) AND p.DISCHARGE IN(9)
       and h.hdc_regist=1
       and TIMESTAMPDIFF(year,p.BIRTH,n.DATE_SERV)=12) z
       GROUP BY areacode
       WITH ROLLUP";

        $cmd = $conn->createCommand($sql);
        $cmd->bindValue(':start_d', $model->start_d);
        $cmd->bindValue(':end_d', $model->end_d);
        $data = $cmd->queryAll();

        return $this->render('index', [
            'model' => $model,
            'data' => $data,
        ]);
    }

}
