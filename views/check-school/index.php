<?php
?>


<?php
$this->title = 'ตรวจสอบโรงเรียน';
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="panel panel-info">
    <div class="panel-heading"> <i class="fa fa-th-list" aria-hidden="true"></i></div>
    <div class="panel-body">

<table  class="datatables table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
            <tr class="info">
                <th>ลำดับ</th>
                <th style="text-align:left">รหัส</th>
                <th style="text-align:left">หน่วยบริการ</th>
                <th style="text-align:left">โรงเรียนทั้งหมด</th>

            </tr>
        </thead>
        <!-- <tfoot>
            <tr>
                <th></th>
            </tr>
        </tfoot> -->
        <tbody>

        <?php
if ($data) {
    foreach ($data as $k => $v) {
        echo '<tr>';
        echo '<td>' . ($k + 1) . '</td>';
        echo '<td>' . $v['hoscode'] . '</td>';
        echo '<td>' . $v['hosname'] . '</td>';
        echo '<td>' . $v['schoolname'] . '</td>';
        echo '</tr>';
    }
    ;
}

?>

        </tbody>
    </table>
</div>
</div>








