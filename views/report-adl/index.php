<?php
/* @var $this yii\web\View */
use app\models\Campur;
use app\models\Cchangwat;
use app\models\Cmastercup;
use app\models\Ctambon;
use app\models\Cyear;
use kartik\depdrop\DepDrop;
use kartik\form\ActiveForm;
use kartik\grid\GridView;
use kartik\select2\Select2;
use rmrevin\yii\fontawesome\FA;
use yii\bootstrap\Html;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use yii\widgets\Pjax;
?>

<?php
$this->title = 'ผู้สูงอายุจำแนกตามความสามารถในการทำกิจวัตรประจำวัน(ADL) (' . $model->ageMin . ' - ' . $model->ageMax . 'ปี)';
$this->params['breadcrumbs'][] = $this->title;
?>


<?php
//Mastercup
$mastercupName = $model->mastercup != null ? Cmastercup::getMastercupname($model->mastercup)
: Campur::GetAmpurname($model->ampurcode) . Ctambon::GetTambonname($model->tamboncode);
?>


<?php
$form = ActiveForm::begin([
    'validateOnSubmit' => true,
    // 'type' => ActiveForm::TYPE_HORIZONTAL,
    // 'fieldConfig' => ['autoPlaceholder' => true],
    // 'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_MEDIUM],
]);
?>


<div class="panel panel-info">
    <div class="panel-heading"></div>
    <div class="panel-body">

<div class="row">

 <div class="col-md-3">
            <?php
echo $form->field($model, 'byear')->widget(Select2::classname(), [
    'data' => Cyear::GetList(),
    'options' => ['placeholder' => 'กรุณาเลือก...'],
    'pluginOptions' => [
        // 'allowClear' => true,
    ],
]);
?>
</div>

 <div class="col-md-3">
            <?php
echo $form->field($model, 'changwatcode')->widget(Select2::classname(), [
    'data' => Cchangwat::GetListCode($model->changwatcode),
    'options' => ['placeholder' => 'กรุณาเลือก...'],
    'pluginOptions' => [
        // 'allowClear' => true,
    ],
]);
?>
 </div>




 </div>   <!--end row-->


<div class="row">

<div class="col-md-3">
            <?php
echo $form->field($model, 'ampurcode')->widget(Select2::classname(), [
    'data' => Campur::GetList($model->changwatcode),
    'options' => ['placeholder' => '-----ทั้งหมด-----', 'id' => 'ampurcode'],
    'pluginOptions' => [
        'allowClear' => true,
    ],
]);

?>
</div>

<div class="col-md-3" style="text-align: left;">
<?php
echo $form->field($model, 'ampurstyle')->widget(DepDrop::classname(), [
    'data' => $this->context->Ampurstyledata($model->ampurstyle),
    'options' => ['prompt' => '-----ค่าเริ่มต้น-----', 'id' => 'ampurstyle'],
    'type' => DepDrop::TYPE_SELECT2,
    'select2Options' => ['pluginOptions' => ['allowClear' => true]],
    'pluginOptions' => [
        'depends' => ['ampurcode'],
        'placeholder' => '-----ค่าเริ่มต้น-----',
        'url' => Url::to(['depdropampurstyle']),
        'loadingText' => 'กรุณารอซักครู่ ...',
    ],
]);

?>
</div>

<div class="col-md-3" style="text-align: left;">
<?php
echo $form->field($model, 'tamboncode')->widget(DepDrop::classname(), [
    'data' => Ctambon::GetTambon($model->tamboncode),
    'options' => ['prompt' => '-----ทั้งหมด-----', 'id' => 'tamboncode'],
    'type' => DepDrop::TYPE_SELECT2,
    'select2Options' => ['pluginOptions' => ['allowClear' => true]],
    'pluginOptions' => [
        'depends' => ['ampurcode', 'ampurstyle'],
        'placeholder' => '-----ทั้งหมด-----',
        'url' => Url::to(['depdroptambon']),
        'loadingText' => 'กรุณารอซักครู่ ...',
    ],
]);

?>
</div>


</div>   <!--end row-->


<div class="row">


<div class="col-md-3">
            <?php
echo $form->field($model, 'mastercup')->widget(Select2::classname(), [
    'data' => Cmastercup::GetMastercup($model->changwatcode),
    'options' => ['placeholder' => '-----ทั้งหมด-----'],
    'pluginOptions' => [
        'allowClear' => true,
    ],
]);

?>
</div>

<div class="col-md-2">
<?php //echo $form->field($model, 'ageMin')->textInput()?>
</div>
<div class="col-md-2">
<?php //echo $form->field($model, 'ageMax')->textInput()?>
</div>

</div>   <!--end row-->

<div class="row">
<div class="col-md-1" style="text-align: center;">
<div class="form-group">
        <?=Html::submitButton(FA::icon('search-plus') . ' ตกลง', ['class' => 'btn btn-primary', 'name' => 'select-button'])?>
    </div>
</div>
</div>   <!--end row-->


</div>
</div>



<?php ActiveForm::end();?>

<div class="report">

<?php Pjax::begin();?>
<?=GridView::widget([
    'dataProvider' => $dataProvider,
    'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
    'responsiveWrap' => false,
    // 'showPageSummary' => true,
    'striped' => true,
    'hover' => true,
    // 'floatHeader' => true,
    // 'floatOverflowContainer' => true,
    // 'resizableColumnsOptions' => ['resizeFromBody' => true],
    'panel' => [
        'type' => 'info',
        'heading' => '<i class="fa fa-th-list" aria-hidden="true"></i> ',
        'before' => $this->title . ' จ. ' . Cchangwat::getChangwatName($model->changwatcode)
        . $mastercupName
        . ' ปีงบ ' . ((int) $model->byear + 543),
        'beforeOptions' => ['style' => 'text-align:center;color:DodgerBlue;font-weight:bold;font-size:16px;'],
        'footer' => '<p>Template :: HDC</p>
        <p>หมายเหตุ :: ประเมินจากผู้สูงอายุไทย อายุ' . ' (' . $model->ageMin . ' - ' . $model->ageMax . 'ปี) ' . 'ในเขตพื้นที่รับผิดชอบ
        - เป็นทะเบียนแบบสะสม ไม่ว่าจะถูกประเมินปีใดสถานพยาบาลใดก็ตาม จะนำผลครั้งสุดท้ายที่ถูกประเมินมารายงาน (ทะเบียน Register)
        - ตัดความซ้ำซ้อนและเชื่อมโยงประวัติด้วยเลขบัตรประชาชน 1 คนจะมีเพียง 1 record ด้วยเลขบัตรประชาชน</p>
        <p>แฟ้มที่ใช้ในการประเมิน SPECIALPP รหัสที่ใช้ในการประเมิน คือ
        - รหัส 1B1280 ติดสังคม (ADL 12-20 คะแนน)
        - รหัส 1B1281 ติดบ้าน (ADL 5-11 คะแนน)
        - รหัส 1B1282 ภาวะติดเตียง (ADL 0-4 คะแนน)</p>
        <p> วันที่ประมวลผล :: ' . Yii::$app->thaidate->Date(date("Y-m-d"), 'n') . '</p>',
        'footerOptions' => ['style' => 'color:Gray;font-size:14px;'],
        // 'after'=>'after',

    ],
    'beforeHeader' => [
        [
            'columns' => [

                ['content' => '',
                    'options' => ['class' => 'info'],
                ],
                ['content' => '',
                    'options' => ['class' => 'info'],
                ],
                ['content' => 'จำนวนผู้สูงอายุได้รับการคัดกรอง',
                    'options' => ['colspan' => 5, 'class' => 'info text-center'],
                ],
                ['content' => '',
                'options' => ['class' => 'info'],
            ],
               
            ],
        ],

    ],

    'toolbar' => [

        // '{toggleData}',
        '{export}',
        ['content' =>
            Html::button('SQL', [
                'data' => [
                    'toggle' => 'modal',
                    'target' => '#sqlModal'],
                'class' => 'btn btn-info',

            ]),

        ],

    ],
    'columns' => [
        // [
        //     'class' => 'kartik\grid\SerialColumn',
        //     'headerOptions' => ['class' => 'info'],
        // ],
        // [
        //     'header' => 'รหัส',
        //     'value' => 'areacode',
        //     'headerOptions' => ['class' => 'info text-center'],
        //     'contentOptions' => ['class' => 'text-left'],
        // ],
        [
            'attribute' => $this->context->getReportTypeName($reportType),
            'value' => function ($v) {
                return $v['areacode'] == null && $v['areaname']!=null?'รวมทั้งหมด':$v['areacode'].' '.$v['areaname'] ;
            },
            'headerOptions' => ['class' => 'info text-center'],
            'contentOptions' => function ($v) {
                return $v['areacode'] !== null ? ['class' => 'warning']
                : ['class' => 'success', 'style' => 'font-weight:bold;font-size:18px;'];
            },
        ],
        //All
        [
            'header' => 'จำนวนผู้งสูงอายุทั้งหมด',
            'value' => 'target',
            'headerOptions' => ['class' => 'info text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'pageSummary' => true,
            'pageSummaryFunc' => GridView::F_SUM,
            'format' => ['decimal', 0],
        ],
        [
            'header' => 'ติดสังคม<br>(ADL 12-20)',
            'value' => 'result1',
            'headerOptions' => ['class' => 'info text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'format' => ['decimal', 0],
        ],
        [
            'header' => 'ติดบ้าน<br>(ADL 5-11)',
            'value' => 'result2',
            'headerOptions' => ['class' => 'info text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'format' => ['decimal', 0],
        ],
        [
            'header' => 'ติดเตียง<br>(ADL 0-4)',
            'value' => 'result3',
            'headerOptions' => ['class' => 'info text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'format' => ['decimal', 0],
        ],
        [
            'header' => 'รวม',
            'value' => 'result',
            'headerOptions' => ['class' => 'info text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'format' => ['decimal', 0],
            'pageSummary' => true,
            'pageSummaryFunc' =>GridView::F_SUM ,
        ],
        [
            'header' => 'ร้อยละ',
            'value' => 'percent',
            'headerOptions' => ['class' => 'info text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'format' => ['decimal', 2],
            'pageSummary' => true,
            'pageSummaryFunc' => GridView::F_AVG,
        ],
        [
            'header' => 'ยังไม่ได้รับการคัดกรอง',
            'value' => function($v){
                return $v['target']-$v['result'];
            },
            'headerOptions' => ['class' => 'info text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'format' => ['decimal', 0],
        ],

    ],


]);?>
<?php Pjax::end();?>

</div>

<!-- Modal SQL -->
<?php
Modal::begin([
    'header' => '<h2 class="text-primary">คำสั่ง SQL</h2>',
    'id' => 'sqlModal',
    'footer' => Html::button('ปิด', ['class' => "btn btn-danger", 'data-dismiss' => "modal"]),
    'size' => 'modal-lg',
    // 'clientOptions'=>['style'=>'background-color: #F2F2F2;']
]);

echo nl2br('<code style="color:black;background-color:white;">' . $sql . '</code>' . '<hr>' . '<code class="text-success"> ค่าตัวแปร :: ' . $sqlParams . '</code>');

Modal::end();
?>



