<?php
/* @var $this yii\web\View */
use app\models\Campur;
use app\models\Cchangwat;
use app\models\Cmastercup;
use app\models\Ctambon;
use app\models\Cyear;
use kartik\depdrop\DepDrop;
use kartik\form\ActiveForm;
use kartik\grid\GridView;
use kartik\select2\Select2;
use rmrevin\yii\fontawesome\FA;
use yii\bootstrap\Html;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use yii\widgets\Pjax;
?>

<?php
$this->title = 'ร้อยละหญิงตั้งครรภ์ได้รับการฝากครรภ์ครั้งแรกก่อนหรือเท่ากับ 12 สัปดาห์ (' . $model->ageMin . ' - ' . $model->ageMax . 'ปี)';
$this->params['breadcrumbs'][] = $this->title;
?>


<?php
//Mastercup
$mastercupName = $model->mastercup != null ? Cmastercup::getMastercupname($model->mastercup)
: Campur::GetAmpurname($model->ampurcode) . Ctambon::GetTambonname($model->tamboncode);
?>


<?php
$form = ActiveForm::begin([
    'validateOnSubmit' => true,
    // 'type' => ActiveForm::TYPE_HORIZONTAL,
    // 'fieldConfig' => ['autoPlaceholder' => true],
    // 'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_MEDIUM],
]);
?>


<div class="panel panel-info">
    <div class="panel-heading"></div>
    <div class="panel-body">

<div class="row">

 <div class="col-md-3">
            <?php
echo $form->field($model, 'byear')->widget(Select2::classname(), [
    'data' => Cyear::GetList(),
    'options' => ['placeholder' => 'กรุณาเลือก...'],
    'pluginOptions' => [
        // 'allowClear' => true,
    ],
]);
?>
</div>

 <div class="col-md-3">
            <?php
echo $form->field($model, 'changwatcode')->widget(Select2::classname(), [
    'data' => Cchangwat::GetListCode($model->changwatcode),
    'options' => ['placeholder' => 'กรุณาเลือก...'],
    'pluginOptions' => [
        // 'allowClear' => true,
    ],
]);
?>
 </div>




 </div>   <!--end row-->


<div class="row">

<div class="col-md-3">
            <?php
echo $form->field($model, 'ampurcode')->widget(Select2::classname(), [
    'data' => Campur::GetList($model->changwatcode),
    'options' => ['placeholder' => '-----ทั้งหมด-----', 'id' => 'ampurcode'],
    'pluginOptions' => [
        'allowClear' => true,
    ],
]);

?>
</div>

<div class="col-md-3" style="text-align: left;">
<?php
echo $form->field($model, 'ampurstyle')->widget(DepDrop::classname(), [
    'data' => $this->context->Ampurstyledata($model->ampurstyle),
    'options' => ['prompt' => '-----ค่าเริ่มต้น-----', 'id' => 'ampurstyle'],
    'type' => DepDrop::TYPE_SELECT2,
    'select2Options' => ['pluginOptions' => ['allowClear' => true]],
    'pluginOptions' => [
        'depends' => ['ampurcode'],
        'placeholder' => '-----ค่าเริ่มต้น-----',
        'url' => Url::to(['depdropampurstyle']),
        'loadingText' => 'กรุณารอซักครู่ ...',
    ],
]);

?>
</div>

<div class="col-md-3" style="text-align: left;">
<?php
echo $form->field($model, 'tamboncode')->widget(DepDrop::classname(), [
    'data' => Ctambon::GetTambon($model->tamboncode),
    'options' => ['prompt' => '-----ทั้งหมด-----', 'id' => 'tamboncode'],
    'type' => DepDrop::TYPE_SELECT2,
    'select2Options' => ['pluginOptions' => ['allowClear' => true]],
    'pluginOptions' => [
        'depends' => ['ampurcode', 'ampurstyle'],
        'placeholder' => '-----ทั้งหมด-----',
        'url' => Url::to(['depdroptambon']),
        'loadingText' => 'กรุณารอซักครู่ ...',
    ],
]);

?>
</div>


</div>   <!--end row-->


<div class="row">


<div class="col-md-3">
            <?php
echo $form->field($model, 'mastercup')->widget(Select2::classname(), [
    'data' => Cmastercup::GetMastercup($model->changwatcode),
    'options' => ['placeholder' => '-----ทั้งหมด-----'],
    'pluginOptions' => [
        'allowClear' => true,
    ],
]);

?>
</div>

<div class="col-md-2">
<?php //echo $form->field($model, 'ageMin')->textInput()?>
</div>
<div class="col-md-2">
<?php //echo $form->field($model, 'ageMax')->textInput()?>
</div>

</div>   <!--end row-->

<div class="row">
<div class="col-md-1" style="text-align: center;">
<div class="form-group">
        <?=Html::submitButton(FA::icon('search-plus') . ' ตกลง', ['class' => 'btn btn-primary', 'name' => 'select-button'])?>
    </div>
</div>
</div>   <!--end row-->


</div>
</div>



<?php ActiveForm::end();?>

<div class="report">

<?php Pjax::begin();?>
<?=GridView::widget([
    'dataProvider' => $dataProvider,
    'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
    'responsiveWrap' => false,
    // 'showPageSummary' => true,
    'striped' => true,
    'hover' => true,
    // 'floatHeader' => true,
    // 'floatOverflowContainer' => true,
    // 'resizableColumnsOptions' => ['resizeFromBody' => true],
    'panel' => [
        'type' => 'info',
        'heading' => '<i class="fa fa-th-list" aria-hidden="true"></i> ',
        'before' => $this->title . ' จ. ' . Cchangwat::getChangwatName($model->changwatcode)
        . $mastercupName
        . ' ปีงบ ' . ((int) $model->byear + 543),
        'beforeOptions' => ['style' => 'text-align:center;color:DodgerBlue;font-weight:bold;font-size:16px;'],
        'footer' => '<p>Template :: HDC</p>
        <p>B หมายถึง จำนวนหญิงไทยในเขตรับผิดชอบที่สิ้นสุดการตั้งครรภ์ทั้งหมด (ฐานข้อมูล 43 แฟ้ม) LABOR</p>
        <p>A หมายถึง จำนวนหญิงตาม B' . ' (' . $model->ageMin . ' - ' . $model->ageMax . 'ปี) ' . 'ที่ฝากครรภ์ครั้งแรกเมื่ออายุครรภ์ <= 12 สัปดาห์ (ข้อมูลจากสมุดสีชมพูบันทึกลงใน 43 แฟ้ม) ANC</p>
        <p> วันที่ประมวลผล :: ' . Yii::$app->thaidate->Date(date("Y-m-d"), 'n') . '</p>',
        'footerOptions' => ['style' => 'color:Gray;font-size:14px;'],
        // 'after'=>'after',

    ],
    'beforeHeader' => [
        [
            'columns' => [
                ['content' => '',
                    'options' => ['class' => 'info'],
                ],
                ['content' => 'รวมทั้งปีงบประมาณ',
                    'options' => ['colspan' => 3, 'class' => 'info text-center'],
                ],
                ['content' => 'ไตรมาส 1',
                    'options' => ['colspan' => 3, 'class' => 'info text-center'],
                ],
                ['content' => 'ไตรมาส 2',
                    'options' => ['colspan' => 3, 'class' => 'info text-center'],
                ],
                ['content' => 'ไตรมาส 3',
                    'options' => ['colspan' => 3, 'class' => 'info text-center'],
                ],
                ['content' => 'ไตรมาส 4',
                    'options' => ['colspan' => 3, 'class' => 'info text-center'],
                ],
            ],
        ],

    ],

    'toolbar' => [

        // '{toggleData}',
        '{export}',
        ['content' =>
            Html::button('SQL', [
                'data' => [
                    'toggle' => 'modal',
                    'target' => '#sqlModal'],
                'class' => 'btn btn-info',

            ]),

        ],

    ],
    'columns' => [
        // [
        //     'class' => 'kartik\grid\SerialColumn',
        //     'headerOptions' => ['class' => 'info'],
        // ],
        // [
        //     'header' => 'รหัส',
        //     'value' => 'areacode',
        //     'headerOptions' => ['class' => 'info text-center'],
        //     'contentOptions' => ['class' => 'text-left'],
        // ],
        [
            'attribute' => $this->context->getReportTypeName($reportType),
            'value' => function ($v) {
                return $v['areacode'] == null && $v['areaname']!=null?'รวมทั้งหมด':$v['areacode'].' '.$v['areaname'] ;
            },
            'headerOptions' => ['class' => 'info text-center'],
            'contentOptions' => function ($v) {
                return $v['areacode'] !== null ? ['class' => 'warning']
                : ['class' => 'success', 'style' => 'font-weight:bold;font-size:18px;'];
            },
        ],
        //All
        [
            'attribute' => 'B',
            'value' => 'target',
            'headerOptions' => ['class' => 'info text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'pageSummary' => true,
            'pageSummaryFunc' => GridView::F_SUM,
            'format' => ['decimal', 0],
        ],
        [
            'attribute' => 'A',
            'value' => 'result',
            'headerOptions' => ['class' => 'info text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'pageSummary' => true,
            'pageSummaryFunc' => GridView::F_SUM,
            'format' => ['decimal', 0],
        ],
        [
            'attribute' => 'ร้อยละ',
            'value' => 'percent',
            'headerOptions' => ['class' => 'info text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'format' => ['decimal', 2],
            'pageSummary' => true,
            'pageSummaryFunc' => function($summaryCellA,$summaryCellB){
                return $summaryCellB/$summaryCellA*100;
            },
        ],
        //T1
        [
            'attribute' => 'B',
            'value' => 'targetq1',
            'headerOptions' => ['class' => 'info text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'format' => ['decimal', 0],
        ],
        [
            'attribute' => 'A',
            'value' => 'resultq1',
            'headerOptions' => ['class' => 'info text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'format' => ['decimal', 0],
        ],
        [
            'attribute' => 'ร้อยละ',
            'value' => 'percentq1',
            'headerOptions' => ['class' => 'info text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'format' => ['decimal', 2],

        ],
        //T2
        [
            'attribute' => 'B',
            'value' => 'targetq2',
            'headerOptions' => ['class' => 'info text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'format' => ['decimal', 0],

        ],
        [
            'attribute' => 'A',
            'value' => 'resultq2',
            'headerOptions' => ['class' => 'info text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'format' => ['decimal', 0],
        ],
        [
            'attribute' => 'ร้อยละ',
            'value' => 'percentq2',
            'headerOptions' => ['class' => 'info text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'format' => ['decimal', 2],
        ],
        //T3
        [
            'attribute' => 'B',
            'value' => 'targetq3',
            'headerOptions' => ['class' => 'info text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'format' => ['decimal', 0],
        ],
        [
            'attribute' => 'A',
            'value' => 'resultq3',
            'headerOptions' => ['class' => 'info text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'format' => ['decimal', 0],
        ],
        [
            'attribute' => 'ร้อยละ',
            'value' => 'percentq3',
            'headerOptions' => ['class' => 'info text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'format' => ['decimal', 2],
        ],
        //T4
        [
            'attribute' => 'B',
            'value' => 'targetq4',
            'headerOptions' => ['class' => 'info text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'format' => ['decimal', 0],
        ],
        [
            'attribute' => 'A',
            'value' => 'resultq4',
            'headerOptions' => ['class' => 'info text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'format' => ['decimal', 0],
        ],
        [
            'attribute' => 'ร้อยละ',
            'value' => 'percentq4',
            'headerOptions' => ['class' => 'info text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'format' => ['decimal', 2],
        ],

    ],


]);?>
<?php Pjax::end();?>

</div>

<!-- Modal SQL -->
<?php
Modal::begin([
    'header' => '<h2 class="text-primary">คำสั่ง SQL</h2>',
    'id' => 'sqlModal',
    'footer' => Html::button('ปิด', ['class' => "btn btn-danger", 'data-dismiss' => "modal"]),
    'size' => 'modal-lg',
    // 'clientOptions'=>['style'=>'background-color: #F2F2F2;']
]);

echo nl2br('<code style="color:black;background-color:white;">' . $sql . '</code>' . '<hr>' . '<code class="text-success"> ค่าตัวแปร :: ' . $sqlParams . '</code>');

Modal::end();
?>



