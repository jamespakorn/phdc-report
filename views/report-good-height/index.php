
<?php
/* @var $this yii\web\View */
use app\models\Campur;
use app\models\Cchangwat;
use app\models\Cmastercup;
use app\models\Ctambon;
use app\models\Cyear;
use kartik\depdrop\DepDrop;
use kartik\form\ActiveForm;
use kartik\grid\GridView;
use kartik\select2\Select2;
use rmrevin\yii\fontawesome\FA;
use yii\bootstrap\Html;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use yii\widgets\Pjax;

?>

<?php
$this->title = 'เด็กวัยเรียนสูงดีสมส่วน (' . $this->context->getClassName($model->class) . ')';
$this->params['breadcrumbs'][] = $this->title;
?>

<?php
//Mastercup
$mastercupName = $model->mastercup != null ? Cmastercup::getMastercupname($model->mastercup)
: Campur::GetAmpurname($model->ampurcode) . Ctambon::GetTambonname($model->tamboncode);
?>


<?php
$form = ActiveForm::begin([
    'validateOnSubmit' => true,
    // 'type' => ActiveForm::TYPE_HORIZONTAL,
    // 'fieldConfig' => ['autoPlaceholder' => true],
    // 'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_MEDIUM],
]);
?>


<div class="panel panel-info">
    <div class="panel-heading"></div>
    <div class="panel-body">

<div class="row">

 <div class="col-md-3">
    <?php
echo Html::tag('label', 'ปีการศึกษา', ['class' => 'reportform-byear']);
echo $form->field($model, 'byear')->label(false)->widget(Select2::classname(), [
    'data' => Cyear::GetList(),
    'options' => ['placeholder' => 'กรุณาเลือก...'],
    'pluginOptions' => [
        // 'allowClear' => true,
    ],
]);
?>
</div>

 <div class="col-md-3">
            <?php
echo $form->field($model, 'changwatcode')->widget(Select2::classname(), [
    'data' => Cchangwat::GetListCode($model->changwatcode),
    'options' => ['placeholder' => 'กรุณาเลือก...'],
    'pluginOptions' => [
        // 'allowClear' => true,
    ],
]);
?>
 </div>




 </div>   <!--end row-->


<div class="row">

<div class="col-md-3">
            <?php
echo $form->field($model, 'ampurcode')->widget(Select2::classname(), [
    'data' => Campur::GetList($model->changwatcode),
    'options' => ['placeholder' => '-----ทั้งหมด-----', 'id' => 'ampurcode'],
    'pluginOptions' => [
        'allowClear' => true,
    ],
]);

?>
</div>

<div class="col-md-3" style="text-align: left;">
<?php
echo $form->field($model, 'ampurstyle')->widget(DepDrop::classname(), [
    'data' => $this->context->Ampurstyledata($model->ampurstyle),
    'options' => ['prompt' => '-----ค่าเริ่มต้น-----', 'id' => 'ampurstyle'],
    'type' => DepDrop::TYPE_SELECT2,
    'select2Options' => ['pluginOptions' => ['allowClear' => true]],
    'pluginOptions' => [
        'depends' => ['ampurcode'],
        'placeholder' => '-----ค่าเริ่มต้น-----',
        'url' => Url::to(['depdropampurstyle']),
        'loadingText' => 'กรุณารอซักครู่ ...',
    ],
]);

?>
</div>

<div class="col-md-3" style="text-align: left;">
<?php
echo $form->field($model, 'tamboncode')->widget(DepDrop::classname(), [
    'data' => Ctambon::GetTambon($model->tamboncode),
    'options' => ['prompt' => '-----ทั้งหมด-----', 'id' => 'tamboncode'],
    'type' => DepDrop::TYPE_SELECT2,
    'select2Options' => ['pluginOptions' => ['allowClear' => true]],
    'pluginOptions' => [
        'depends' => ['ampurcode', 'ampurstyle'],
        'placeholder' => '-----ทั้งหมด-----',
        'url' => Url::to(['depdroptambon']),
        'loadingText' => 'กรุณารอซักครู่ ...',
    ],
]);

?>
</div>


</div>   <!--end row-->


<div class="row">


<div class="col-md-3">
            <?php
echo $form->field($model, 'mastercup')->widget(Select2::classname(), [
    'data' => Cmastercup::GetMastercup($model->changwatcode),
    'options' => ['placeholder' => '-----ทั้งหมด-----'],
    'pluginOptions' => [
        'allowClear' => true,
    ],
]);

?>
</div>

<div class="col-md-3">
            <?php
echo $form->field($model, 'class')->widget(Select2::classname(), [
    'data' => $this->context->getClass(),
    // 'options' => ['placeholder' => '-----ทั้งหมด-----'],
    // 'pluginOptions' => [
    //     'allowClear' => true,
    // ],
]);

?>
</div>



</div>   <!--end row-->

<div class="row">
<div class="col-md-1" style="text-align: center;">
<div class="form-group">
        <?=Html::submitButton(FA::icon('search-plus') . ' ตกลง', ['class' => 'btn btn-primary', 'name' => 'select-button'])?>
    </div>
</div>
</div>   <!--end row-->


</div>
</div>



<?php ActiveForm::end();?>

<div class="report">

<?php Pjax::begin();?>
<?=GridView::widget([
    'dataProvider' => $dataProvider,
    'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
    'responsiveWrap' => false,
    // 'showPageSummary' => true,
    'striped' => true,
    'hover' => true,
    // 'floatHeader' => true,
    // 'floatOverflowContainer' => true,
    // 'resizableColumnsOptions' => ['resizeFromBody' => true],
    'panel' => [
        'type' => 'info',
        'heading' => '<i class="fa fa-th-list" aria-hidden="true"></i> ',
        'before' => $this->title . ' จ. ' . Cchangwat::getChangwatName($model->changwatcode)
        . $mastercupName
        . ' ปีการศึกษา ' . ((int) $model->byear + 543),
        'beforeOptions' => ['style' => 'text-align:center;color:DodgerBlue;font-weight:bold;font-size:16px;'],
        'footer' => '<p>Template :: HDC</p>
        <p>B : เด็กวัยเรียน ' . $this->context->getClassName($model->class) . ' ที่ชั่งนำหนักวัดส่วนสูง</p>
        <p>A : เด็กวัยเรียน ' . $this->context->getClassName($model->class) . ' สูงดีสมส่วน </p>
        <p> ***หมายเหตุ ข้อมูลที่ใช้ ' . Yii::$app->thaidate->Date($model->start_d, 's') . ' - ' . Yii::$app->thaidate->Date($model->end_d, 's') . '</p>
         <p> วันที่ประมวลผล :: ' . Yii::$app->thaidate->Date(date("Y-m-d"), 'n') . '</p>',
        'footerOptions' => ['style' => 'color:Gray;font-size:14px;'],
        // 'after'=>'after',

    ],

    'beforeHeader' => [
        [
            'columns' => [
                ['content' => '',
                    'options' => ['class' => 'info'],
                ],
                ['content' => '',
                    'options' => ['class' => 'info'],
                ],
                ['content' => '',
                    'options' => ['class' => 'info'],
                ],
                ['content' => '',
                    'options' => ['class' => 'info'],
                ],
                ['content' => 'เทอม1<br>พ.ค.'.((int) $model->byear + 543).'- ก.ค. '.((int) $model->byear + 543),
                    'options' => ['colspan' => 10, 'class' => 'info text-center'],
                ],
                ['content' => 'เทอม2<br>ต.ค.'.((int) $model->byear + 543).'- ม.ค. '.((int) $model->byear + 544),
                    'options' => ['colspan' => 10, 'class' => 'info text-center'],
                ],

            ],
        ],

    ],

    'toolbar' => [

        // '{toggleData}',
        '{export}',
        ['content' =>
            Html::button('SQL', [
                'data' => [
                    'toggle' => 'modal',
                    'target' => '#sqlModal'],
                'class' => 'btn btn-info',

            ]),

        ],

    ],
    'columns' => [
        // [
        //     'class' => 'kartik\grid\SerialColumn',
        //     'headerOptions' => ['class' => 'info'],
        // ],
        // [
        //     'header' => 'รหัส',
        //     'value' => 'areacode',
        //     'headerOptions' => ['class' => 'info text-center'],
        //     'contentOptions' => ['class' => 'text-left'],
        //     'hAlign' => 'left',
        //     'vAlign' => 'middle',
        // ],
        [
            'attribute' => $this->context->getReportTypeName($reportType),
            'value' => function ($v) {
                return $v['areacode'] == null && $v['areaname']!=null?'รวมทั้งหมด':$v['areacode'].' '.$v['areaname'] ;
            },
            'headerOptions' => ['class' => 'info'],
            'contentOptions' => function ($v) {
                return $v['areacode'] !== null ? ['class' => 'warning']
                : ['class' => 'success', 'style' => 'font-weight:bold;font-size:18px;'];
            },
            'hAlign' => 'left',
            'vAlign' => 'middle',
            'width' => '100px',
        ],
        [
            'header' => 'เด็ก ' . $this->context->getClassName($model->class),
            'value' => 'B0',
            'hAlign' => 'right',
            'vAlign' => 'middle',
            'headerOptions' => ['class' => 'info'],
            'format' => ['decimal', 0],
            'pageSummary' => true,
            'pageSummaryFunc' => GridView::F_SUM,
        ],
        //Term1
        [
            'header' => 'ชั่งน้ำหนัก<br>วัดส่วนสูง<br>(B1)',
            'value' => 'B1T1',
            'hAlign' => 'right',
            'vAlign' => 'middle',
            'headerOptions' => ['class' => 'info'],
            'format' => ['decimal', 0],
            'pageSummary' => true,
            'pageSummaryFunc' => GridView::F_SUM,
        ],
        [
            'header' => '%',
            'value' => function ($v) {
                return $v['B0'] != 0 ? $v['B1T1'] / $v['B0'] * 100 : 0;
            },
            'hAlign' => 'right',
            'vAlign' => 'middle',
            'headerOptions' => ['class' => 'success'],
            'format' => ['decimal', 2],
        ],
        [
            'header' => 'สูงดี<br>สมส่วน<br>(A1)',
            'value' => 'A1T1',
            'hAlign' => 'right',
            'vAlign' => 'middle',
            'headerOptions' => ['class' => 'info '],
            'format' => ['decimal', 0],
            'pageSummary' => true,
            'pageSummaryFunc' => GridView::F_SUM,
        ],
        [
            'header' => '%',
            'value' => function ($v) {
                return $v['B1T1'] != 0 ? $v['A1T1'] / $v['B1T1'] * 100 : 0;
            },
            'hAlign' => 'right',
            'vAlign' => 'middle',
            'headerOptions' => ['class' => 'success'],
            'format' => ['decimal', 2],
        ],
        [
            'header' => 'ผอม<br>(A2)',
            'value' => 'A2T1',
            'hAlign' => 'right',
            'vAlign' => 'middle',
            'headerOptions' => ['class' => 'info '],
            'format' => ['decimal', 0],
            'pageSummary' => true,
            'pageSummaryFunc' => GridView::F_SUM,
        ],
        [
            'header' => '%',
            'value' => function ($v) {
                return $v['B1T1'] != 0 ? $v['A2T1'] / $v['B1T1'] * 100 : 0;
            },
            'hAlign' => 'right',
            'vAlign' => 'middle',
            'headerOptions' => ['class' => 'success'],
            'format' => ['decimal', 2],
        ],
        [
            'header' => 'เริ่มอ้วน<br>และอ้วน<br>(A3)',
            'value' => 'A3T1',
            'hAlign' => 'right',
            'vAlign' => 'middle',
            'headerOptions' => ['class' => 'info '],
            'format' => ['decimal', 0],
            'pageSummary' => true,
            'pageSummaryFunc' => GridView::F_SUM,
        ],
        [
            'header' => '%',
            'value' => function ($v) {
                return $v['B1T1'] != 0 ? $v['A3T1'] / $v['B1T1'] * 100 : 0;
            },
            'hAlign' => 'right',
            'vAlign' => 'middle',
            'headerOptions' => ['class' => 'success'],
            'format' => ['decimal', 2],
        ],
        [
            'header' => 'เตี้ย<br>(A4)',
            'value' => 'A4T1',
            'hAlign' => 'right',
            'vAlign' => 'middle',
            'headerOptions' => ['class' => 'info'],
            'format' => ['decimal', 0],
            'pageSummary' => true,
            'pageSummaryFunc' => GridView::F_SUM,
        ],
        [
            'header' => '%',
            'value' => function ($v) {
                return $v['B1T1'] != 0 ? $v['A4T1'] / $v['B1T1'] * 100 : 0;
            },
            'hAlign' => 'right',
            'vAlign' => 'middle',
            'headerOptions' => ['class' => 'success'],
            'format' => ['decimal', 2],
        ],

        //Term2
        [
            'header' => 'ชั่งน้ำหนัก<br>วัดส่วนสูง<br>(B1)',
            'value' => 'B1T2',
            'hAlign' => 'right',
            'vAlign' => 'middle',
            'headerOptions' => ['class' => 'info'],
            'format' => ['decimal', 0],
            'pageSummary' => true,
            'pageSummaryFunc' => GridView::F_SUM,
        ],
        [
            'header' => '%',
            'value' => function ($v) {
                return $v['B0'] != 0 ? $v['B1T2'] / $v['B0'] * 100 : 0;
            },
            'hAlign' => 'right',
            'vAlign' => 'middle',
            'headerOptions' => ['class' => 'success'],
            'format' => ['decimal', 2],
        ],
        [
            'header' => 'สูงดี<br>สมส่วน<br>(A1)',
            'value' => 'A1T2',
            'hAlign' => 'right',
            'vAlign' => 'middle',
            'headerOptions' => ['class' => 'info '],
            'format' => ['decimal', 0],
            'pageSummary' => true,
            'pageSummaryFunc' => GridView::F_SUM,
        ],
        [
            'header' => '%',
            'value' => function ($v) {
                return $v['B1T2'] != 0 ? $v['A1T2'] / $v['B1T2'] * 100 : 0;
            },
            'hAlign' => 'right',
            'vAlign' => 'middle',
            'headerOptions' => ['class' => 'success'],
            'format' => ['decimal', 2],
        ],
        [
            'header' => 'ผอม<br>(A2)',
            'value' => 'A2T2',
            'hAlign' => 'right',
            'vAlign' => 'middle',
            'headerOptions' => ['class' => 'info '],
            'format' => ['decimal', 0],
            'pageSummary' => true,
            'pageSummaryFunc' => GridView::F_SUM,
        ],
        [
            'header' => '%',
            'value' => function ($v) {
                return $v['B1T2'] != 0 ? $v['A2T2'] / $v['B1T2'] * 100 : 0;
            },
            'hAlign' => 'right',
            'vAlign' => 'middle',
            'headerOptions' => ['class' => 'success'],
            'format' => ['decimal', 2],
        ],
        [
            'header' => 'เริ่มอ้วน<br>และอ้วน<br>(A3)',
            'value' => 'A3T2',
            'hAlign' => 'right',
            'vAlign' => 'middle',
            'headerOptions' => ['class' => 'info '],
            'format' => ['decimal', 0],
            'pageSummary' => true,
            'pageSummaryFunc' => GridView::F_SUM,
        ],
        [
            'header' => '%',
            'value' => function ($v) {
                return $v['B1T2'] != 0 ? $v['A3T2'] / $v['B1T2'] * 100 : 0;
            },
            'hAlign' => 'right',
            'vAlign' => 'middle',
            'headerOptions' => ['class' => 'success'],
            'format' => ['decimal', 2],
        ],
        [
            'header' => 'เตี้ย<br>(A4)',
            'value' => 'A4T2',
            'hAlign' => 'right',
            'vAlign' => 'middle',
            'headerOptions' => ['class' => 'info'],
            'format' => ['decimal', 0],
            'pageSummary' => true,
            'pageSummaryFunc' => GridView::F_SUM,
        ],
        [
            'header' => '%',
            'value' => function ($v) {
                return $v['B1T2'] != 0 ? $v['A4T2'] / $v['B1T2'] * 100 : 0;
            },
            'hAlign' => 'right',
            'vAlign' => 'middle',
            'headerOptions' => ['class' => 'success'],
            'format' => ['decimal', 2],
        ],

        

    ],

]);?>
<?php Pjax::end();?>

</div>

<!-- Modal SQL -->
<?php
Modal::begin([
    'header' => '<h2 class="text-primary">คำสั่ง SQL</h2>',
    'id' => 'sqlModal',
    'footer' => Html::button('ปิด', ['class' => "btn btn-danger", 'data-dismiss' => "modal"]),
    'size' => 'modal-lg',
    // 'clientOptions'=>['style'=>'background-color: #F2F2F2;']
]);

echo nl2br('<code style="color:black;background-color:white;">' . $sql . '</code>' . '<hr>' . '<code class="text-success"> ค่าตัวแปร :: ' . $sqlParams . '</code>');

Modal::end();
?>







