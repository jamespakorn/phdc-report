<?php
use app\models\Cchangwat;

/* @var $this yii\web\View */

$this->title =Yii::$app->name;
?>
<div class="site-index">

    <div class="jumbotron">
        <h1><?=Yii::$app->name ?></h1>

        <p class="lead"> <?='ระบบรายงาน HDC สสจ.' . Cchangwat::getChangwatName(Yii::$app->params['provinceCode'])?></p>

        <p><a class="btn btn-lg btn-success" href="#">ยินดีต้อนรับ</a></p>
    </div>


</div>
