
<?php
/* @var $this yii\web\View */
use app\models\Campur;
use app\models\Cchangwat;
use app\models\Cmastercup;
use app\models\Ctambon;
use app\models\Cyear;
use kartik\depdrop\DepDrop;
use kartik\form\ActiveForm;
use kartik\grid\GridView;
use kartik\select2\Select2;
use rmrevin\yii\fontawesome\FA;
use yii\bootstrap\Html;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use yii\widgets\Pjax;

?>

<?php
$this->title = 'เด็กนักเรียนปราศจากฟันผุในฟันแท้(Caries Free) (' . $this->context->getClassName($model->class) . ')';
$this->params['breadcrumbs'][] = $this->title;
?>

<?php
//Mastercup
$mastercupName = $model->mastercup != null ? Cmastercup::getMastercupname($model->mastercup)
: Campur::GetAmpurname($model->ampurcode) . Ctambon::GetTambonname($model->tamboncode);
?>


<?php
$form = ActiveForm::begin([
    'validateOnSubmit' => true,
    // 'type' => ActiveForm::TYPE_HORIZONTAL,
    // 'fieldConfig' => ['autoPlaceholder' => true],
    // 'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_MEDIUM],
]);
?>


<div class="panel panel-info">
    <div class="panel-heading"></div>
    <div class="panel-body">

<div class="row">

 <div class="col-md-3">
            <?php
echo Html::tag('label', 'ปีการศึกษา', ['class' => 'reportform-byear']);
echo $form->field($model, 'byear')->label(false)->widget(Select2::classname(), [
    'data' => Cyear::GetList(),
    'options' => ['placeholder' => 'กรุณาเลือก...'],
    'pluginOptions' => [
        // 'allowClear' => true,
    ],
]);
?>
</div>

 <div class="col-md-3">
            <?php
echo $form->field($model, 'changwatcode')->widget(Select2::classname(), [
    'data' => Cchangwat::GetListCode($model->changwatcode),
    'options' => ['placeholder' => 'กรุณาเลือก...'],
    'pluginOptions' => [
        // 'allowClear' => true,
    ],
]);
?>
 </div>




 </div>   <!--end row-->


<div class="row">

<div class="col-md-3">
            <?php
echo $form->field($model, 'ampurcode')->widget(Select2::classname(), [
    'data' => Campur::GetList($model->changwatcode),
    'options' => ['placeholder' => '-----ทั้งหมด-----', 'id' => 'ampurcode'],
    'pluginOptions' => [
        'allowClear' => true,
    ],
]);

?>
</div>

<div class="col-md-3" style="text-align: left;">
<?php
echo $form->field($model, 'ampurstyle')->widget(DepDrop::classname(), [
    'data' => $this->context->Ampurstyledata($model->ampurstyle),
    'options' => ['prompt' => '-----ค่าเริ่มต้น-----', 'id' => 'ampurstyle'],
    'type' => DepDrop::TYPE_SELECT2,
    'select2Options' => ['pluginOptions' => ['allowClear' => true]],
    'pluginOptions' => [
        'depends' => ['ampurcode'],
        'placeholder' => '-----ค่าเริ่มต้น-----',
        'url' => Url::to(['depdropampurstyle']),
        'loadingText' => 'กรุณารอซักครู่ ...',
    ],
]);

?>
</div>

<div class="col-md-3" style="text-align: left;">
<?php
echo $form->field($model, 'tamboncode')->widget(DepDrop::classname(), [
    'data' => Ctambon::GetTambon($model->tamboncode),
    'options' => ['prompt' => '-----ทั้งหมด-----', 'id' => 'tamboncode'],
    'type' => DepDrop::TYPE_SELECT2,
    'select2Options' => ['pluginOptions' => ['allowClear' => true]],
    'pluginOptions' => [
        'depends' => ['ampurcode', 'ampurstyle'],
        'placeholder' => '-----ทั้งหมด-----',
        'url' => Url::to(['depdroptambon']),
        'loadingText' => 'กรุณารอซักครู่ ...',
    ],
]);

?>
</div>


</div>   <!--end row-->


<div class="row">


<div class="col-md-3">
            <?php
echo $form->field($model, 'mastercup')->widget(Select2::classname(), [
    'data' => Cmastercup::GetMastercup($model->changwatcode),
    'options' => ['placeholder' => '-----ทั้งหมด-----'],
    'pluginOptions' => [
        'allowClear' => true,
    ],
]);


?>
</div>

<div class="col-md-3">
            <?php
echo $form->field($model, 'cupstyle')->widget(Select2::classname(), [
    'data' =>['01'=>'หน่วยบริการ','02'=>'โรงเรียน'],
    'options' => ['placeholder' => '-----ทั้งหมด-----'],
    'pluginOptions' => [
        'allowClear' => true,
    ],
]);
?>
</div>

<div class="col-md-3">
            <?php
echo $form->field($model, 'class')->widget(Select2::classname(), [
    'data' => $this->context->getClass(),
    // 'options' => ['placeholder' => '-----ทั้งหมด-----'],
    // 'pluginOptions' => [
    //     'allowClear' => true,
    // ],
]);

?>
</div>



</div>   <!--end row-->

<div class="row">
<div class="col-md-1" style="text-align: center;">
<div class="form-group">
        <?=Html::submitButton(FA::icon('search-plus') . ' ตกลง', ['class' => 'btn btn-primary', 'name' => 'select-button'])?>
    </div>
</div>
</div>   <!--end row-->


</div>
</div>



<?php ActiveForm::end();?>

<div class="report">

<?php Pjax::begin();?>
<?=GridView::widget([
    'dataProvider' => $dataProvider,
    'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
    'responsiveWrap' => false,
    // 'showPageSummary' => true,
    'striped' => true,
    'hover' => true,
    // 'floatHeader' => true,
    // 'floatOverflowContainer' => true,
    // 'resizableColumnsOptions' => ['resizeFromBody' => true],
    'panel' => [
        'type' => 'info',
        'heading' => '<i class="fa fa-th-list" aria-hidden="true"></i> ',
        'before' => $this->title . ' จ. ' . Cchangwat::getChangwatName($model->changwatcode)
        . $mastercupName
        . ' ปีการศึกษา ' . ((int) $model->byear + 543),
        'beforeOptions' => ['style' => 'text-align:center;color:DodgerBlue;font-weight:bold;font-size:16px;'],
        'footer' => '<p>Template :: HPC11</p>
        <p>B : เด็ก ' . $this->context->getClassName($model->class) . ' ที่มีชื่อในโรงเรียนในเขตรับผิดชอบ
         ที่ได้รับการตรวจฟันทั้งปาก + วางแผนการรักษา ลงข้อมูลในแฟ้ม dental และได้รับการตรวจสุขภาพช่องปาก
         โดยทันตบุคลากร (provider type = 02, 06) ไม่นับซ้ำ (distinct) hospcode+pid และ pid ใช้ '
        . Yii::$app->thaidate->Date($model->start_d, 's') . ' - ' . Yii::$app->thaidate->Date($model->end_d, 's') .
        ' ที่ปรับปรุงข้อมูลล่าสุด (d_update) , ไม่นับซ้ำ (distinct) hospcode + pid  และแฟ้ม dental มี provider type=02,06
        เงื่อนไขคุณภาพแฟ้ม dental
        PFILLING+PEXTRACT+PCARIES <=' . $model->ppp . ' ,PTEETH = ' . $model->pteeth1 . ' - ' . $model->pteeth2 . ' และ PCARIES+PFILLING<=PTEETH</p>
        <p>A : นับจำนวนเด็กจากรายการข้อมูล 2 (B) ที่ได้รับการตรวจฟันทั้งปาก + วางแผนการรักษา
        ลงข้อมูลในแฟ้ม dental และได้รับการตรวจสุขภาพช่องปากโดยทันตบุคลากร (provider type = 02, 06)
         ไม่นับซ้ำ (distinct) hospcode+pid และ pid ใช้ ' . Yii::$app->thaidate->Date($model->start_d, 's') . ' - ' . Yii::$app->thaidate->Date($model->end_d, 's') .
        ' ที่มี เงื่อนไขคือ pcaries+pfilling+pextract = 0 </p>
         <p> วันที่ประมวลผล :: ' . Yii::$app->thaidate->Date(date("Y-m-d"), 'n') . '</p>',
        'footerOptions' => ['style' => 'color:Gray;font-size:14px;'],
        // 'after'=>'after',

    ],

    'toolbar' => [

        // '{toggleData}',
        '{export}',
        ['content' =>
            Html::button('SQL', [
                'data' => [
                    'toggle' => 'modal',
                    'target' => '#sqlModal'],
                'class' => 'btn btn-info',

            ]),

        ],

    ],
    'columns' => [
        // [
        //     'class' => 'kartik\grid\SerialColumn',
        //     'headerOptions' => ['class' => 'info'],
        // ],
        // [
        //     'header' => 'รหัส',
        //     'value' => 'areacode',
        //     'headerOptions' => ['class' => 'info text-center'],
        //     'contentOptions' => ['class' => 'text-left'],
        // ],

        [
            'attribute' => $this->context->getReportTypeName($reportType),
            'value' => function ($v) {
                return $v['areacode'] == null && $v['areaname']!=null?'รวมทั้งหมด':$v['areacode'].' '.$v['areaname'] ;
            },
            'headerOptions' => ['class' => 'info text-center'],
            'contentOptions' => function ($v) {
                return $v['areacode'] !== null ? ['class' => 'warning']
                : ['class' => 'success', 'style' => 'font-weight:bold;font-size:18px;'];
            },
        ],
        //All
        [
            'header' => 'B',
            'value' => 'target',
            'headerOptions' => ['class' => 'info text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'format' => ['decimal', 0],
        ],
        [
            'header' => 'A',
            'value' => 'result',
            'headerOptions' => ['class' => 'info text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'format' => ['decimal', 0],

        ],
        [
            'header' => 'ร้อยละ',
            'value' => 'percent',
            'headerOptions' => ['class' => 'info text-center'],
            'contentOptions' => ['class' => 'text-center'],
            'format' => ['decimal', 2],
        ],

    ],

]);?>
<?php Pjax::end();?>

</div>

<!-- Modal SQL -->
<?php
Modal::begin([
    'header' => '<h2 class="text-primary">คำสั่ง SQL</h2>',
    'id' => 'sqlModal',
    'footer' => Html::button('ปิด', ['class' => "btn btn-danger", 'data-dismiss' => "modal"]),
    'size' => 'modal-lg',
    // 'clientOptions'=>['style'=>'background-color: #F2F2F2;']
]);

echo nl2br('<code style="color:black;background-color:white;">' . $sql . '</code>' . '<hr>' . '<code class="text-success"> ค่าตัวแปร :: ' . $sqlParams . '</code>');

Modal::end();
?>







