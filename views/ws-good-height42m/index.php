<?php
/* @var $this yii\web\View */
use app\models\Campur;
use app\models\Cmastercup;
use app\models\Ctambon;
use app\models\Cyear;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use rmrevin\yii\fontawesome\FA;
use yii\bootstrap\Html;
?>

<?php
$this->title = 'ร้อยละเด็กอายุ 42 เดือน สูงดีสมส่วน';
$this->params['breadcrumbs'][] = $this->title;
?>


<?php
//Mastercup
$mastercupName = $model->mastercup != null ? Cmastercup::getMastercupname($model->mastercup)
: Campur::GetAmpurname($model->ampurcode) . Ctambon::GetTambonname($model->tamboncode);
?>


<?php
$form = ActiveForm::begin([
    'validateOnSubmit' => true,
    'type' => ActiveForm::TYPE_HORIZONTAL,
    // 'fieldConfig' => ['autoPlaceholder' => true],
    'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_MEDIUM],
]);
?>


<div class="panel panel-info">
    <div class="panel-heading"></div>
    <div class="panel-body">

<div class="row">

 <div class="col-md-4">
            <?php
echo $form->field($model, 'byear')->widget(Select2::classname(), [
    'data' => Cyear::GetList(),
    'options' => ['placeholder' => 'กรุณาเลือก...'],
    'pluginOptions' => [
        // 'allowClear' => true,
    ],
]);
?>
</div>


<div class="col-md-1" style="text-align: center;">
<div class="form-group">
        <?=Html::submitButton(FA::icon('search-plus') . ' ตกลง', ['class' => 'btn btn-primary', 'name' => 'select-button'])?>
    </div>
</div>

</div>


</div>
</div>



<?php ActiveForm::end();?>


<div class="panel panel-info">
    <div class="panel-heading"> <i class="fa fa-th-list" aria-hidden="true"></i></div>
    <div class="panel-body">

<table  class="datatables table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
            <tr class="info">
                <!-- <th>ลำดับ</th> -->
                <th>หน่วยบริการ</th>
                <th>เป้าหมาย</th>
                <th>ผลงาน</th>
                <th>ร้อยละ</th>

            </tr>
        </thead>
        <!-- <tfoot>
            <tr>
                <th></th>
            </tr>
        </tfoot> -->
        <tbody>

        <?php
if ($data) {
    foreach ($data as $k => $v) {
        if ($v['areacode'] != null) {
            echo '<tr>';
            echo '<td>' . $v['areacode'] . ' ' . $v['areaname'] . '</td>';
            echo '<td>' . number_format($v['target']) . '</td>';
            echo '<td>' . number_format($v['result']) . '</td>';
            echo '<td>' . number_format($v['percent'], 2) . '</td>';
            echo '</tr>';
        } else {
            echo '<tr style="background-color:skyblue">';
            echo '<td >รวมทั้งหมด</td>';
            echo '<td>' . number_format($v['target']) . '</td>';
            echo '<td>' . number_format($v['result']) . '</td>';
            echo '<td>' . number_format($v['percent'], 2) . '</td>';
            echo '</tr>';

        }

    }
    ;
}

?>

        </tbody>
    </table>
</div>
</div>




