<?php
use app\models\Cyear;
// use kartik\form\ActiveForm;
use yii\bootstrap\ActiveForm;
use kartik\select2\Select2;
use yii\bootstrap\Html;
use app\models\School;
?>


<?php
$this->title = 'ตรวจสอบนักเรียน';
$this->params['breadcrumbs'][] = $this->title;
?>


<?php
$form = ActiveForm::begin([
    // 'type' => ActiveForm::TYPE_HORIZONTAL,
    //'fieldConfig' => ['autoPlaceholder' => true],
    // 'formConfig' => ['labelSpan' => 2, 'deviceSize' => ActiveForm::SIZE_MEDIUM],
]);
?>

    <div class="panel panel-default">
    <div class="panel-heading"></div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-3">
            <?php
            echo $form->field($model, 'eyear')->widget(Select2::classname(), [
                'data' => Cyear::GetList(),
                'options' => ['placeholder' => 'กรุณาเลือก...'],
                'pluginOptions' => [
                    // 'allowClear' => true,
                ],
            ]);
            ?>
            </div>
            <div class="col-md-8">
            <?php
            echo $form->field($model, 'school')->widget(Select2::classname(), [
                'data' =>School::GetList(),
                'options' => ['placeholder' => 'กรุณาเลือก...'],
                'pluginOptions' => [
                    'allowClear' => true,
                ],
            ]);
            ?>
            </div>
    </div> <!--end row-->

    <div class="row">

            <div class="col-md-12" style="text-align: left;">
                <div class="form-group">
                    <?=Html::submitButton('<i class="glyphicon glyphicon-zoom-in"></i> ค้นหา', ['class' => 'btn btn-primary', 'name' => 'select-button'])?>
                </div>
            </div>
        </div>
    </div>
    </div>

   <?php ActiveForm::end();?>


<div class="panel panel-info">
    <div class="panel-heading"> <i class="fa fa-th-list" aria-hidden="true"></i><?=' ปีการศึกษา ' . ($model->eyear + 543)?></div>
    <div class="panel-body">

<table  class="datatables table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
            <tr class="info">
                <th>#</th>
                <th style="text-align:left">รหัส</th>
                <th style="text-align:left">หน่วยบริการ</th>
                <th style="text-align:center">ศพด.</th>
                <th style="text-align:center">อ.1</th>
                <th style="text-align:center">อ.2</th>
                <th style="text-align:center">อ.3</th>
                <th style="text-align:center">ป.1</th>
                <th style="text-align:center">ป.2</th>
                <th style="text-align:center">ป.3</th>
                <th style="text-align:center">ป.4</th>
                <th style="text-align:center">ป.5</th>
                <th style="text-align:center">ป.6</th>
                <th style="text-align:center">ม.1</th>
                <th style="text-align:center">ม.2</th>
                <th style="text-align:center">ม.3</th>
                <th style="text-align:center">ม.4</th>
                <th style="text-align:center">ม.5</th>
                <th style="text-align:center">ม.6</th>
                <th style="text-align:center">รวม</th>
            </tr>
        </thead>
        <!-- <tfoot>
            <tr>
                <th></th>
            </tr>
        </tfoot> -->
        <tbody>

        <?php
if ($students) {

    foreach ($students as $k => $v) {
        echo '<tr>';
        echo '<td>' . ($k + 1) . '</td>';
        echo '<td>' . $v['hospcode'] . '</td>';
        echo '<td>' . $v['hosname'] . '</td>';
        echo '<td style="text-align:center">' . number_format($v['00']) . '</td>';
        echo '<td style="text-align:center">' . number_format($v['01']) . '</td>';
        echo '<td style="text-align:center">' . number_format($v['02']) . '</td>';
        echo '<td style="text-align:center">' . number_format($v['03']) . '</td>';
        echo '<td style="text-align:center">' . number_format($v['11']) . '</td>';
        echo '<td style="text-align:center">' . number_format($v['12']) . '</td>';
        echo '<td style="text-align:center">' . number_format($v['13']) . '</td>';
        echo '<td style="text-align:center">' . number_format($v['21']) . '</td>';
        echo '<td style="text-align:center">' . number_format($v['22']) . '</td>';
        echo '<td style="text-align:center">' . number_format($v['23']) . '</td>';
        echo '<td style="text-align:center">' . number_format($v['31']) . '</td>';
        echo '<td style="text-align:center">' . number_format($v['32']) . '</td>';
        echo '<td style="text-align:center">' . number_format($v['33']) . '</td>';
        echo '<td style="text-align:center">' . number_format($v['41']) . '</td>';
        echo '<td style="text-align:center">' . number_format($v['42']) . '</td>';
        echo '<td style="text-align:center">' . number_format($v['43']) . '</td>';
        echo '<td style="text-align:center">' . number_format($v['total']) . '</td>';
        echo '</tr>';
    }

}

?>

        </tbody>
    </table>
</div>
</div>








