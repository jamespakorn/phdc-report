<?php

/* @var $this \yii\web\View */
/* @var $content string */

// use app\assets\AppAsset;
use app\assets\ThemesAsset;
use yii\helpers\Html;

// AppAsset::register($this);
ThemesAsset::register($this);
?>
<?php $this->beginPage()?>
<!DOCTYPE html>
<html lang="<?=Yii::$app->language?>">
<head>
    <meta charset="<?=Yii::$app->charset?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="Description" content="PHDC Report">
    <meta name="KeyWords" content="PHDC Report">
    <?=Html::csrfMetaTags()?>
    <title><?=Html::encode($this->title)?></title>
    <?php $this->head()?>
</head>
<body>

<?php $this->beginBody()?>

<div class="wrap">

    <div class="container">

        <?=$content?>
    </div>
</div>



<?php $this->endBody()?>
</body>
</html>
<?php $this->endPage()?>

