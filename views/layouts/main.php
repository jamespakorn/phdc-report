<?php

/* @var $this \yii\web\View */
/* @var $content string */

// use app\assets\AppAsset;
use app\assets\ThemesAsset;
use app\models\Cchangwat;
use app\widgets\Alert;
use rmrevin\yii\fontawesome\FA;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use app\assets\DatatablesAsset;

// AppAsset::register($this);
ThemesAsset::register($this);
DatatablesAsset::register($this);
?>
<?php $this->beginPage()?>
<!DOCTYPE html>
<html lang="<?=Yii::$app->language?>">
<head>
    <link rel="shortcut icon" href="<?=Yii::getAlias('@web')?>/img/favicon.ico" />
    <meta charset="<?=Yii::$app->charset?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="Description" content="PHDC Report">
    <meta name="KeyWords" content="PHDC Report">
    <?=Html::csrfMetaTags()?>
    <title><?=Html::encode($this->title)?></title>
    <?php $this->head()?>
</head>
<body>

<?php $this->beginBody()?>

<?=$this->render('/options/_datatables.php');?>

<div class="wrap">
    <?php
NavBar::begin([
    'brandLabel' => FA::icon('hospital-o') . ' ' . Yii::$app->name . ' สสจ.' . Cchangwat::getChangwatName(Yii::$app->params['provinceCode']),
    'brandUrl' => Yii::$app->homeUrl,
    'options' => [
        'class' => 'navbar-default ', //navbar-fixed-top
    ],
]);

$childage = [
    '<li class="divider"></li>',
    ['label' => FA::icon('caret-square-o-right', ['class' => 'text-primary']) . ' ร้อยละเด็กอายุ 42 เดือน สูงดีสมส่วน', 'url' => ['/ws-good-height42m']],

];
$schoolage = [
    '<li class="divider"></li>',
    ['label' => FA::icon('caret-square-o-right', ['class' => 'text-primary']) .  ' ร้อยละเด็กอายุ 12 ปี สูงดีสมส่วน', 'url' => ['/ws-good-height12y']],
    '<li class="divider"></li>',
    ['label' => FA::icon('caret-square-o-right', ['class' => 'text-primary']) . ' เด็กวัยเรียนสูงดีสมส่วน(โรงเรียน)', 'url' => ['/report-good-height']],

];

$teens = [
    '<li class="divider"></li>',
    ['label' => FA::icon('caret-square-o-right', ['class' => 'text-primary']) . ' หญิงตั้งครรภ์อายุน้อยกว่า 20 ปี ได้รับการฝากครรภ์ครั้งแรก <= 12 สัปดาห์ ', 'url' => ['/report-anc12']],
    ['label' => FA::icon('caret-square-o-right', ['class' => 'text-primary']) . ' ทารกแรกเกิดน้ำหนักน้อยกว่า 2,500 กรัมและมีมารดาอายุน้อยกว่า 20 ปี', 'url' => ['/report-momchild2500']],

];

$working = [
    '<li class="divider"></li>',
    ['label' => FA::icon('caret-square-o-right', ['class' => 'text-primary']) . ' วัยทำงาน 35 ปี มีค่าดัชนีมวลกายปกติ ', 'url' => ['/report-bmi']],

];

$ageing = [
    '<li class="divider"></li>',
    ['label' => FA::icon('caret-square-o-right', ['class' => 'text-primary']) . ' ผู้สูงอายุ 74 ปี จำแนกตามความสามารถในการทำกิจวัตรประจำวัน(ADL) ', 'url' => ['/report-adl']],

];

$dent = [
    '<li class="divider"></li>',
    ['label' => FA::icon('caret-square-o-right', ['class' => 'text-primary']) . ' เด็กนักเรียนได้รับการตรวจช่องปาก(โรงเรียน)', 'url' => ['/report-oral-examination']],
    ['label' => FA::icon('caret-square-o-right', ['class' => 'text-primary']) . ' เด็กนักเรียนปราศจากฟันผุในฟันแท้(Caries Free)(โรงเรียน)', 'url' => ['/report-caries-free']],
    ['label' => FA::icon('caret-square-o-right', ['class' => 'text-primary']) . ' เด็กนักเรียนมีฟันดีไม่มีผุ(Cavity Free)(โรงเรียน) ', 'url' => ['/report-cavity-free']],

];

$check = [
    '<li class="divider"></li>',
    ['label' => FA::icon('caret-square-o-right', ['class' => 'text-primary']) . ' ตรวจสอบนักเรียน', 'url' => ['/check-student']],
    ['label' => FA::icon('caret-square-o-right', ['class' => 'text-primary']) . ' ตรวจสอบโรงเรียน', 'url' => ['/check-school']],
];

echo Nav::widget([
    'options' => ['class' => 'navbar-nav navbar-right'],
    'encodeLabels' => false,
    'items' => [
        // ['label' => FA::icon('home') . ' หน้าแรก', 'url' => ['/site/index']],
        ['label' => FA::icon('users') . ' แม่และเด็ก', 'items' => $childage],
        ['label' => FA::icon('users') . ' วัยเรียน', 'items' => $schoolage],
        ['label' => FA::icon('venus-mars') . ' วัยรุ่น', 'items' => $teens],
        ['label' => FA::icon('user-circle') . ' วัยทำงาน', 'items' => $working],
        ['label' => FA::icon('wheelchair') . ' ผู้สูงอายุ', 'items' => $ageing],
        ['label' => FA::icon('linode') . ' ทันตกรรม', 'items' => $dent],
        ['label' => FA::icon('check') . ' ติดตามข้อมูล', 'items' => $check],
        // ['label' => 'Contact', 'url' => ['/site/contact']],
        // Yii::$app->user->isGuest ? (
        //     ['label' => 'Login', 'url' => ['/site/login']]
        // ) : (
        //     '<li>'
        //     . Html::beginForm(['/site/logout'], 'post')
        //     . Html::submitButton(
        //         'Logout (' . Yii::$app->user->identity->username . ')',
        //         ['class' => 'btn btn-link logout']
        //     )
        //     . Html::endForm()
        //     . '</li>'
        // ),
    ],
]);
NavBar::end();
?>

    <div class="container">
        <?=Breadcrumbs::widget([
    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
])?>
        <?=Alert::widget()?>
        <?=$content?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p style="text-align:center;">&copy; 
        <?='สสจ.' . Cchangwat::getChangwatName(Yii::$app->params['provinceCode']) . ' ' . date('Y')?>
        (1.62.7.5)
        </p>

        <!-- <p class="pull-right"><?//=Yii::powered()?></p> -->
    </div>
</footer>

<?php $this->endBody()?>
</body>
</html>
<?php $this->endPage()?>
<style>

    ul,li,p,a,h1,h2,h3,h4,h5,h6,
    a.btn,button.btn,span,th,td,
    div,select{
        font-family: 'Sriracha', cursive ;
    }


</style>
