<?php
$script = <<< JS

// Datatables
$('.datatables').DataTable( {

        scrollY:			'450px',
        scrollX:			true,
        scrollCollapse: true,
        // DeferRender: true,
        // AutoWidth:		true,
        paging:         false,
        // info:				false,
        // searching: false,
        // aaSorting: [],
      columnDefs: [
            { width: '200px', targets: 2 }

        ],
        // fixedColumns: true,
        // fixedColumns:   {
        //     leftColumns: 1
        // },
        language:  {
                "sProcessing": "กำลังดำเนินการ...",
                "sLengthMenu": "แสดง _MENU_ แถว",
                "sZeroRecords": "ไม่พบข้อมูล",
                "sInfo": "แสดง _START_ ถึง _END_ จาก _TOTAL_ แถว",
                "sInfoEmpty": "แสดง 0 ถึง 0 จาก 0 แถว",
                "sInfoFiltered": "(กรองข้อมูล _MAX_ ทุกแถว)",
                "sInfoPostFix": "",
                "sSearch": "ค้นหา: ",
                "sUrl": "",
                "oPaginate": {
                    "sFirst": "หน้าแรก",
                    "sPrevious": "ก่อนหน้า",
                    "sNext": "ถัดไป",
                    "sLast": "หน้าสุดท้าย"
                }

        },
        // dom: 'lBfrtip',
        dom: 'Bfrtip',

        buttons: [
            {
            'extend': 'colvis',
            'text':'<span class="text-primary"><i class="fa fa-th-list" aria-hidden="true"></i> Show/Hide</span>'
            },
            {
            'extend': 'copyHtml5',
            'text':'<span class="text-primary"><i class="fa fa-files-o" aria-hidden="true"></i> Copy</span>'
            },
            {
            'extend': 'excelHtml5',
            'text':'<span class="text-primary"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Excel</span>'
            },
            // 'csvHtml5',
            // 'pdfHtml5',

        ],
    });



JS;
$this->registerJs($script);
